import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:osama_test/utlis/binding.dart';
import 'package:osama_test/view/Payments.dart';
import 'package:osama_test/view/ambulance.dart';
import 'package:osama_test/view/appointmentsdoctor_screen.dart';
import 'package:osama_test/view/appointmentsdoctorpationt_screen.dart';
import 'package:osama_test/view/appointmentsoptions_scrren.dart';
import 'package:osama_test/view/appointmentssurgerydoctor_screen.dart';

import 'package:osama_test/view/appoitmentsurgery_screen.dart';
import 'package:osama_test/view/authentication/login_screen.dart';
import 'package:osama_test/view/ehr_screen.dart';
import 'package:osama_test/view/home_screen.dart';
import 'package:osama_test/view/patient_screen.dart';
import 'package:osama_test/view/prescriptions_screen.dart';
import 'package:osama_test/view/room_booking.dart';
import 'package:osama_test/view/splash_screen.dart';

import 'global/theme/theme_data/light_theme_data.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.transparent,
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Network Applications Project',
      debugShowCheckedModeBanner: false,
      theme: lightThemeData(),
      initialBinding: Binding(),
      initialRoute: '/login',
      getPages: [
        GetPage(
          name: '/login',
          page: () => const LoginScreen(),
          binding: Binding(),
        ),
         GetPage(
          name: '/AppointmentSurgerydoctorScreen',
          page: () => const AppointmentSurgerydoctorScreen(),
          binding: Binding(),
        ),
         GetPage(
          name: '/AppointmentdoctorpationtScreen',
          page: () => const AppointmentdoctorpationtScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/splash',
          page: () => const Splashscreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/payment',
          page: () => const Payment(),
          binding: Binding(),
        ),
        GetPage(
          name: '/home',
          page: () => const HomeScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/ambulance',
          page: () => const AmbulanceScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/AppointmentsOptions',
          page: () => const AppointmentsOptions(),
          binding: Binding(),
        ),
        GetPage(
          name: '/patientscreen',
          page: () => const PatientScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/ehrscreen',
          page: () => const EhrScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/appointmentsurgeryscreen',
          page: () => const AppointmentSurgeryScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/appointmentdoctorscreen',
          page: () => const AppointmentdoctorScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/roombooking',
          page: () => const RoomBookingScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/prescriptions',
          page: () => const PrescriptionsScreen(),
          binding: Binding(),
        ),
        GetPage(
          name: '/Paymentscreen',
          page: () => const Payment(),
          binding: Binding(),
        ),
      ],
    );
  }
}
