import 'package:flutter/material.dart';

class AppLightColors{
  static const Color primaryColor = Color(0xff24cbc8);
  static const Color secondaryColor = Colors.black;
  static const Color appBarColor = Colors.white;
  static const Color textColor = Colors.black;
  static const Color scaffoldBackgroundColor = Colors.white;
  static  Color captionTextColor = Colors.grey.shade100;
}
