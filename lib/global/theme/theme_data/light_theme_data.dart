import 'package:flutter/material.dart';

import '../app_colors/app_light_colors.dart';

ThemeData lightThemeData() => ThemeData(
  primaryColor: AppLightColors.primaryColor,
  focusColor: AppLightColors.primaryColor,
  fontFamily: 'robot',
  //INPUT DECORATION THEME DATA
  inputDecorationTheme: InputDecorationTheme(
    focusColor: AppLightColors.primaryColor,
    iconColor: Colors.black,
    focusedBorder: OutlineInputBorder(
      borderSide: const BorderSide(color: AppLightColors.primaryColor),
      borderRadius: BorderRadius.circular(10.0),
    ),
    labelStyle: const TextStyle(
      fontSize: 18.0,
      color: Colors.black,
    ),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10.0),
    ),
    floatingLabelBehavior: FloatingLabelBehavior.always,
  ),
  textButtonTheme: TextButtonThemeData(
    style: ButtonStyle(
      foregroundColor:
      MaterialStateProperty.all(AppLightColors.primaryColor),
    ),
  ),
  textTheme: const TextTheme(
    displayLarge: TextStyle(
        fontSize: 32.0,
        fontWeight: FontWeight.bold,
        color: AppLightColors.primaryColor,
        fontFamily: 'roboto'),
  ),
);
