import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../theme/app_colors/app_light_colors.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    Key? key,
    this.title,
    this.backgroundColor,
    this.leading,
  }) : super(key: key);
  final String? title;
  final Color? backgroundColor;
  final Widget? leading;

  @override
  Size get preferredSize => const Size.fromHeight(60);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      systemOverlayStyle: const SystemUiOverlayStyle(
        // Status bar color
        // statusBarColor: Colors.white,

        // Status bar brightness (optional)
        statusBarIconBrightness: Brightness.dark, // For Android (dark icons)
        statusBarBrightness: Brightness.light, // For iOS (dark icons)
      ),
      title: Text(title ?? '' , style: const TextStyle(color: AppLightColors.textColor),),
      backgroundColor: backgroundColor ?? AppLightColors.appBarColor,
      leading: leading,
      elevation: 0,
    );
  }
}

