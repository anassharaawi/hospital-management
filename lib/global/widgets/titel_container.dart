import 'package:flutter/material.dart';
import 'package:osama_test/global/theme/app_colors/app_light_colors.dart';

class TitleContainer extends StatelessWidget {
  final String title;
  final String content;

  const TitleContainer({Key? key, required this.title, required this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 17),
          padding: const EdgeInsets.fromLTRB(20, 40, 20, 20),
          // height: 200,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.grey.shade300,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Text(
            content,
            style: const TextStyle(fontSize: 18),
          ),
        ),
        Align(
          // top: 0,
          // left: 20,
          // right: 100,rrr
          alignment: Alignment.topLeft,
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            width: 200,
            // alignment: Alignment.center,
            decoration: const BoxDecoration(
              // color: AppLightColors.primaryColor,
              color: Colors.blueGrey,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
            ),
            child: Text(
              title,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: const TextStyle(
                fontSize: 18,
                color: Colors.white,
              ),
            ),
          ),
        )
      ],
    );
  }
}



class TitleContainercopy extends StatelessWidget {
  final String title;
  final String content;
    final String content1;


  const TitleContainercopy({Key? key, required this.title, required this.content, required this.content1})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 17),
          padding: const EdgeInsets.fromLTRB(20, 40, 20, 20),
          // height: 200,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Colors.grey.shade300,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: [
              Text(
                content,
                style: const TextStyle(fontSize: 18),
              ),
              
               Text(
                content1,
                style: const TextStyle(fontSize: 18),
              )
            ],
          ),
        ),
        Align(
          // top: 0,
          // left: 20,
          // right: 100,rrr
          alignment: Alignment.topLeft,
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            width: 200,
            // alignment: Alignment.center,
            decoration: const BoxDecoration(
              // color: AppLightColors.primaryColor,
              color: Colors.blueGrey,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
            ),
            child: Text(
              title,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: const TextStyle(
                fontSize: 18,
                color: Colors.white,
              ),
            ),
          ),
        )
      ],
    );
  }
}
