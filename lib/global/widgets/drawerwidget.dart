import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../view/Payments.dart';
import '../../view/ambulance.dart';
import '../../view/prescriptions_screen.dart';
import '../theme/app_colors/app_light_colors.dart';

class DrwerWidget extends StatelessWidget {
  const DrwerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Color.fromARGB(255, 228, 228, 228),
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            const DrawerHeader(
              decoration: BoxDecoration(color: AppLightColors.primaryColor),
              child: Center(
                  child: Text(
                'Hospital Management',
                style: TextStyle(fontSize: 35),
              )),
            ),
            ListTile(
              title: const Text('AmbulanceScreen'),
              onTap: () {
                Get.to(() => const AmbulanceScreen());
              },
            ),
            ListTile(
              title: Text('PrescriptionsScreen'),
              onTap: () {
                Get.to(() => const PrescriptionsScreen());
              },
            ),ListTile(
              title: Text('PaymentScreen'),
              onTap: () {
                Get.to(() => const Payment());
              },
            ),
          ],
        ),
      ),
    );
  }
}
