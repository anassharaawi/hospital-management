import 'package:flutter/material.dart';

class CustomizedFormField extends StatelessWidget {
   CustomizedFormField({
    Key? key,
    required this.textInputType,
    required this.prefixIcon,
    required this.labelText,
    required this.textEditingController,
    required this.validate,
    this.obscureText = false,
    this.enableSuggestions = true,
    this.autocorrect = true,
  }) : super(key: key);

  final TextInputType textInputType;
  final IconData prefixIcon;
  final String labelText;
  final TextEditingController textEditingController;
  final Function validate;
  final bool obscureText, enableSuggestions, autocorrect;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: textEditingController,
      obscureText: obscureText,
      enableSuggestions: enableSuggestions,
      autocorrect: autocorrect,
      style: const TextStyle(fontSize: 18.0, color: Colors.black87),
      keyboardType: textInputType,
      cursorColor: Colors.black87,
      autofocus: true,
      decoration: InputDecoration(
        prefixIcon: Icon(
          prefixIcon,
        ),
        labelText: labelText,

      ),
      validator: (value) => validate(value),
    );
  }
}


class amFormField extends StatelessWidget {
   amFormField({
    Key? key,
    required this.textInputType,
    required this.prefixIcon,
    required this.labelText,
    
    required this.validate,
    this.obscureText = false,
    this.enableSuggestions = true,
    this.autocorrect = true,
  }) : super(key: key);

  final TextInputType textInputType;
  final IconData prefixIcon;
  final String labelText;
  
  final Function validate;
  final bool obscureText, enableSuggestions, autocorrect;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      
      obscureText: obscureText,
      enableSuggestions: enableSuggestions,
      autocorrect: autocorrect,
      style: const TextStyle(fontSize: 18.0, color: Colors.black87),
      keyboardType: textInputType,
      cursorColor: Colors.black87,
      autofocus: true,
      decoration: InputDecoration(
        prefixIcon: Icon(
          prefixIcon,
        ),
        labelText: labelText,

      ),
      validator: (value) => validate(value),
    );
  }
}

class peko extends StatelessWidget {
  const peko({Key? key, this.textData,  this.onTap}) : super(key: key);
  final String? textData;

  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: const EdgeInsets.all(5),
          child: Text(
            "$textData  ",
            maxLines: 10,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontSize: 15,
              color: Colors.black,
            ),
          ),
        )
      ],
    );
  }
}
