// To parse this JSON data, do
//
//     final appointmentsDoctorPationtModel = appointmentsDoctorPationtModelFromJson(jsonString);

import 'dart:convert';

List<AppointmentsDoctorPationtModel> appointmentsDoctorPationtModelFromJson(
        String str) =>
    List<AppointmentsDoctorPationtModel>.from(json
        .decode(str)
        .map((x) => AppointmentsDoctorPationtModel.fromJson(x)));

String appointmentsDoctorPationtModelToJson(
        List<AppointmentsDoctorPationtModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AppointmentsDoctorPationtModel {
  int id;
  int departmentId;
  int userId;
  int doctorId;
  DateTime date;
  String status;
  String description;
  DateTime createdAt;
  DateTime updatedAt;

  AppointmentsDoctorPationtModel({
    required this.id,
    required this.departmentId,
    required this.userId,
    required this.doctorId,
    required this.date,
    required this.status,
    required this.description,
    required this.createdAt,
    required this.updatedAt,
  });

  factory AppointmentsDoctorPationtModel.fromJson(Map<String, dynamic> json) =>
      AppointmentsDoctorPationtModel(
        id: json["id"],
        departmentId: json["department_id"],
        userId: json["user_id"],
        doctorId: json["doctor_id"],
        date: DateTime.parse(json["date"]),
        status: json["status"],
        description: json["description"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "department_id": departmentId,
        "user_id": userId,
        "doctor_id": doctorId,
        "date": date.toIso8601String(),
        "status": status,
        "description": description,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
