// To parse this JSON data, do
//
//     final appointmentsDoctorModel = appointmentsDoctorModelFromJson(jsonString);

import 'dart:convert';

List<AppointmentsDoctorModel> appointmentsDoctorModelFromJson(String str) => List<AppointmentsDoctorModel>.from(json.decode(str).map((x) => AppointmentsDoctorModel.fromJson(x)));

String appointmentsDoctorModelToJson(List<AppointmentsDoctorModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AppointmentsDoctorModel {
    int id;
    DateTime date;
    String status;
    dynamic description;
    DateTime createdAt;
    DateTime updatedAt;
    String doctorFirstName;
    String doctorLastName;
    Department department;

    AppointmentsDoctorModel({
        required this.id,
        required this.date,
        required this.status,
        this.description,
        required this.createdAt,
        required this.updatedAt,
        required this.doctorFirstName,
        required this.doctorLastName,
        required this.department,
    });

    factory AppointmentsDoctorModel.fromJson(Map<String, dynamic> json) => AppointmentsDoctorModel(
        id: json["id"],
        date: DateTime.parse(json["date"]),
        status: json["status"],
        description: json["description"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        doctorFirstName: json["doctor_first_name"],
        doctorLastName: json["doctor_last_name"],
        department: Department.fromJson(json["department"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "date": date.toIso8601String(),
        "status": status,
        "description": description,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "doctor_first_name": doctorFirstName,
        "doctor_last_name": doctorLastName,
        "department": department.toJson(),
    };
}

class Department {
    int id;
    String name;
    String floor;
    DateTime createdAt;
    DateTime updatedAt;

    Department({
        required this.id,
        required this.name,
        required this.floor,
        required this.createdAt,
        required this.updatedAt,
    });

    factory Department.fromJson(Map<String, dynamic> json) => Department(
        id: json["id"],
        name: json["name"],
        floor: json["floor"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "floor": floor,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
    };
}
