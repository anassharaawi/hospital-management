// To parse this JSON data, do
//
//     final prescriptionsModel = prescriptionsModelFromJson(jsonString);

import 'dart:convert';

List<PrescriptionsModel> prescriptionsModelFromJson(String str) => List<PrescriptionsModel>.from(json.decode(str).map((x) => PrescriptionsModel.fromJson(x)));

String prescriptionsModelToJson(List<PrescriptionsModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PrescriptionsModel {
    int id;
    DateTime date;
    String description;
    DateTime createdAt;
    DateTime updatedAt;
    String doctorFirstName;
    String doctorLastName;
    List<MedicalAnalysis> medicalAnalysis;
    List<Radiograph> radiograph;
    List<Medicine> medicine;

    PrescriptionsModel({
        required this.id,
        required this.date,
        required this.description,
        required this.createdAt,
        required this.updatedAt,
        required this.doctorFirstName,
        required this.doctorLastName,
        required this.medicalAnalysis,
        required this.radiograph,
        required this.medicine,
    });

    factory PrescriptionsModel.fromJson(Map<String, dynamic> json) => PrescriptionsModel(
        id: json["id"],
        date: DateTime.parse(json["date"]),
        description: json["description"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        doctorFirstName: json["doctor_first_name"],
        doctorLastName: json["doctor_last_name"],
        medicalAnalysis: List<MedicalAnalysis>.from(json["medical_analysis"].map((x) => MedicalAnalysis.fromJson(x))),
        radiograph: List<Radiograph>.from(json["radiograph"].map((x) => Radiograph.fromJson(x))),
        medicine: List<Medicine>.from(json["medicine"].map((x) => Medicine.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "description": description,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "doctor_first_name": doctorFirstName,
        "doctor_last_name": doctorLastName,
        "medical_analysis": List<dynamic>.from(medicalAnalysis.map((x) => x.toJson())),
        "radiograph": List<dynamic>.from(radiograph.map((x) => x.toJson())),
        "medicine": List<dynamic>.from(medicine.map((x) => x.toJson())),
    };
}

class MedicalAnalysis {
    int id;
    String name;
    String availability;
    int price;
    DateTime createdAt;
    DateTime updatedAt;
    MedicalAnalysisPivot pivot;

    MedicalAnalysis({
        required this.id,
        required this.name,
        required this.availability,
        required this.price,
        required this.createdAt,
        required this.updatedAt,
        required this.pivot,
    });

    factory MedicalAnalysis.fromJson(Map<String, dynamic> json) => MedicalAnalysis(
        id: json["id"],
        name: json["name"],
        availability: json["availability"],
        price: json["price"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        pivot: MedicalAnalysisPivot.fromJson(json["pivot"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "availability": availability,
        "price": price,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "pivot": pivot.toJson(),
    };
}

class MedicalAnalysisPivot {
    int medicalPrescriptionId;
    int medicalAnalysisId;

    MedicalAnalysisPivot({
        required this.medicalPrescriptionId,
        required this.medicalAnalysisId,
    });

    factory MedicalAnalysisPivot.fromJson(Map<String, dynamic> json) => MedicalAnalysisPivot(
        medicalPrescriptionId: json["medical_prescription_id"],
        medicalAnalysisId: json["medical_analysis_id"],
    );

    Map<String, dynamic> toJson() => {
        "medical_prescription_id": medicalPrescriptionId,
        "medical_analysis_id": medicalAnalysisId,
    };
}

class Medicine {
    int id;
    String name;
    String category;
    int sellPrice;
    int quantity;
    String company;
    String description;
    DateTime createdAt;
    DateTime updatedAt;
    MedicinePivot pivot;

    Medicine({
        required this.id,
        required this.name,
        required this.category,
        required this.sellPrice,
        required this.quantity,
        required this.company,
        required this.description,
        required this.createdAt,
        required this.updatedAt,
        required this.pivot,
    });

    factory Medicine.fromJson(Map<String, dynamic> json) => Medicine(
        id: json["id"],
        name: json["name"],
        category: json["category"],
        sellPrice: json["sell_price"],
        quantity: json["quantity"],
        company: json["company"],
        description: json["description"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        pivot: MedicinePivot.fromJson(json["pivot"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "category": category,
        "sell_price": sellPrice,
        "quantity": quantity,
        "company": company,
        "description": description,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "pivot": pivot.toJson(),
    };
}

class MedicinePivot {
    int medicalPrescriptionId;
    int medicineId;
    String dosage;

    MedicinePivot({
        required this.medicalPrescriptionId,
        required this.medicineId,
        required this.dosage,
    });

    factory MedicinePivot.fromJson(Map<String, dynamic> json) => MedicinePivot(
        medicalPrescriptionId: json["medical_prescription_id"],
        medicineId: json["medicine_id"],
        dosage: json["dosage"],
    );

    Map<String, dynamic> toJson() => {
        "medical_prescription_id": medicalPrescriptionId,
        "medicine_id": medicineId,
        "dosage": dosage,
    };
}

class Radiograph {
    int id;
    String name;
    String availability;
    int price;
    DateTime createdAt;
    DateTime updatedAt;
    RadiographPivot pivot;

    Radiograph({
        required this.id,
        required this.name,
        required this.availability,
        required this.price,
        required this.createdAt,
        required this.updatedAt,
        required this.pivot,
    });

    factory Radiograph.fromJson(Map<String, dynamic> json) => Radiograph(
        id: json["id"],
        name: json["name"],
        availability: json["availability"],
        price: json["price"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        pivot: RadiographPivot.fromJson(json["pivot"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "availability": availability,
        "price": price,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "pivot": pivot.toJson(),
    };
}

class RadiographPivot {
    int medicalPrescriptionId;
    int radiographId;

    RadiographPivot({
        required this.medicalPrescriptionId,
        required this.radiographId,
    });

    factory RadiographPivot.fromJson(Map<String, dynamic> json) => RadiographPivot(
        medicalPrescriptionId: json["medical_prescription_id"],
        radiographId: json["radiograph_id"],
    );

    Map<String, dynamic> toJson() => {
        "medical_prescription_id": medicalPrescriptionId,
        "radiograph_id": radiographId,
    };
}
