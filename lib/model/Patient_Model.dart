// To parse this JSON data, do
//
//     final patientDetailsModel = patientDetailsModelFromJson(jsonString);

import 'dart:convert';

PatientDetailsModel patientDetailsModelFromJson(String str) => PatientDetailsModel.fromJson(json.decode(str));

String patientDetailsModelToJson(PatientDetailsModel data) => json.encode(data.toJson());

class PatientDetailsModel {
    int id;
    String firstName;
    String fatherName;
    String lastName;
    String motherName;
    String gender;
    String city;
    String communicationNumber;
    String address;
    DateTime birthdate;
    String nationalNumber;
    String email;
    int roleId;
    int ehrId;

    PatientDetailsModel({
        required this.id,
        required this.firstName,
        required this.fatherName,
        required this.lastName,
        required this.motherName,
        required this.gender,
        required this.city,
        required this.communicationNumber,
        required this.address,
        required this.birthdate,
        required this.nationalNumber,
        required this.email,
        required this.roleId,
        required this.ehrId,
    });

    factory PatientDetailsModel.fromJson(Map<String, dynamic> json) => PatientDetailsModel(
        id: json["id"],
        firstName: json["first_name"],
        fatherName: json["father_name"],
        lastName: json["last_name"],
        motherName: json["mother_name"],
        gender: json["gender"],
        city: json["city"],
        communicationNumber: json["communication_number"],
        address: json["address"],
        birthdate: DateTime.parse(json["birthdate"]),
        nationalNumber: json["national_number"],
        email: json["email"],
        roleId: json["role_id"],
        ehrId: json["ehr_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "first_name": firstName,
        "father_name": fatherName,
        "last_name": lastName,
        "mother_name": motherName,
        "gender": gender,
        "city": city,
        "communication_number": communicationNumber,
        "address": address,
        "birthdate": "${birthdate.year.toString().padLeft(4, '0')}-${birthdate.month.toString().padLeft(2, '0')}-${birthdate.day.toString().padLeft(2, '0')}",
        "national_number": nationalNumber,
        "email": email,
        "role_id": roleId,
        "ehr_id": ehrId,
    };
}
