// To parse this JSON data, do
//
//     final edrModel = edrModelFromJson(jsonString);

import 'dart:convert';

EhrModel edrModelFromJson(String str) => EhrModel.fromJson(json.decode(str));

String edrModelToJson(EhrModel data) => json.encode(data.toJson());

class EhrModel {
  int id;
  int patientId;
  String foodAllergies;
  String medicineAllergies;
  String heartDisease;
  String bloodPressure;
  String diabetic;
  String bleedTendency;
  String surgery;
  String bloodGroup;
  String accident;
  String familyMedicalHistory;
  String currentMedication;
  String femalePregnancy;
  String breastFeeding;
  String personification;
  String symptoms;

  EhrModel({
    required this.id,
    required this.patientId,
    required this.foodAllergies,
    required this.medicineAllergies,
    required this.heartDisease,
    required this.bloodPressure,
    required this.diabetic,
    required this.bleedTendency,
    required this.surgery,
    required this.bloodGroup,
    required this.accident,
    required this.familyMedicalHistory,
    required this.currentMedication,
    required this.femalePregnancy,
    required this.breastFeeding,
    required this.personification,
    required this.symptoms,
  });

  factory EhrModel.fromJson(Map<String, dynamic> json) => EhrModel(
        id: json["id"],
        patientId: json["patient_id"],
        foodAllergies: json["food_allergies"],
        medicineAllergies: json["medicine_allergies"],
        heartDisease: json["heart_disease"],
        bloodPressure: json["blood_pressure"],
        diabetic: json["diabetic"],
        bleedTendency: json["bleed_tendency"],
        surgery: json["surgery"],
        bloodGroup: json["blood_group"],
        accident: json["accident"],
        familyMedicalHistory: json["family_medical_history"],
        currentMedication: json["current_medication"],
        femalePregnancy: json["female_pregnancy"],
        breastFeeding: json["breast_feeding"],
        personification: json["personification"],
        symptoms: json["symptoms"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "patient_id": patientId,
        "food_allergies": foodAllergies,
        "medicine_allergies": medicineAllergies,
        "heart_disease": heartDisease,
        "blood_pressure": bloodPressure,
        "diabetic": diabetic,
        "bleed_tendency": bleedTendency,
        "surgery": surgery,
        "blood_group": bloodGroup,
        "accident": accident,
        "family_medical_history": familyMedicalHistory,
        "current_medication": currentMedication,
        "female_pregnancy": femalePregnancy,
        "breast_feeding": breastFeeding,
        "personification": personification,
        "symptoms": symptoms,
      };
}
