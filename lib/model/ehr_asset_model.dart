import 'dart:convert';

EhrAssetModel ehrAssetModelFromJson(String str) => EhrAssetModel.fromJson(json.decode(str));

String ehrAssetModelToJson(EhrAssetModel data) => json.encode(data.toJson());

class EhrAssetModel {
    int id;
    int ehrId;
    DateTime date;
    String type;
    DateTime createdAt;
    DateTime updatedAt;
    List<Attachment> attachments;

    EhrAssetModel({
        required this.id,
        required this.ehrId,
        required this.date,
        required this.type,
        required this.createdAt,
        required this.updatedAt,
        required this.attachments,
    });

    factory EhrAssetModel.fromJson(Map<String, dynamic> json) => EhrAssetModel(
        id: json["id"],
        ehrId: json["ehr_id"],
        date: DateTime.parse(json["date"]),
        type: json["type"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        attachments: List<Attachment>.from(json["attachments"].map((x) => Attachment.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "ehr_id": ehrId,
        "date": "${date.year.toString().padLeft(4, '0')}-${date.month.toString().padLeft(2, '0')}-${date.day.toString().padLeft(2, '0')}",
        "type": type,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "attachments": List<dynamic>.from(attachments.map((x) => x.toJson())),
    };
}

class Attachment {
    int id;
    int ehrAssetId;
    String src;
    String title;
    DateTime createdAt;
    DateTime updatedAt;

    Attachment({
        required this.id,
        required this.ehrAssetId,
        required this.src,
        required this.title,
        required this.createdAt,
        required this.updatedAt,
    });

    factory Attachment.fromJson(Map<String, dynamic> json) => Attachment(
        id: json["id"],
        ehrAssetId: json["ehr_asset_id"],
        src: json["src"],
        title: json["title"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "ehr_asset_id": ehrAssetId,
        "src": src,
        "title": title,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
    };
}