// To parse this JSON data, do
//
//     final appointmentSurgeryModel = appointmentSurgeryModelFromJson(jsonString);

import 'dart:convert';

List<AppointmentSurgeryModel> appointmentSurgeryModelFromJson(String str) => List<AppointmentSurgeryModel>.from(json.decode(str).map((x) => AppointmentSurgeryModel.fromJson(x)));

String appointmentSurgeryModelToJson(List<AppointmentSurgeryModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AppointmentSurgeryModel {
    int id;
    int userId;
    int doctorId;
    String name;
    String resuscitation;
    String anesthetization;
    String price;
    DateTime date;
    String description;
    DateTime createdAt;
    DateTime updatedAt;
    Doctor doctor;

    AppointmentSurgeryModel({
        required this.id,
        required this.userId,
        required this.doctorId,
        required this.name,
        required this.resuscitation,
        required this.anesthetization,
        required this.price,
        required this.date,
        required this.description,
        required this.createdAt,
        required this.updatedAt,
        required this.doctor,
    });

    factory AppointmentSurgeryModel.fromJson(Map<String, dynamic> json) => AppointmentSurgeryModel(
        id: json["id"],
        userId: json["user_id"],
        doctorId: json["doctor_id"],
        name: json["name"],
        resuscitation: json["resuscitation"],
        anesthetization: json["anesthetization"],
        price: json["price"],
        date: DateTime.parse(json["date"]),
        description: json["description"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        doctor: Doctor.fromJson(json["doctor"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "doctor_id": doctorId,
        "name": name,
        "resuscitation": resuscitation,
        "anesthetization": anesthetization,
        "price": price,
        "date": date.toIso8601String(),
        "description": description,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "doctor": doctor.toJson(),
    };
}

class Doctor {
    int id;
    int departmentId;
    int userId;
    String specialization;
    String academicQualifications;
    String medicalDegree;
    DateTime createdAt;
    DateTime updatedAt;
    User user;

    Doctor({
        required this.id,
        required this.departmentId,
        required this.userId,
        required this.specialization,
        required this.academicQualifications,
        required this.medicalDegree,
        required this.createdAt,
        required this.updatedAt,
        required this.user,
    });

    factory Doctor.fromJson(Map<String, dynamic> json) => Doctor(
        id: json["id"],
        departmentId: json["department_id"],
        userId: json["user_id"],
        specialization: json["specialization"],
        academicQualifications: json["academic_qualifications"],
        medicalDegree: json["medical_degree"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        user: User.fromJson(json["user"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "department_id": departmentId,
        "user_id": userId,
        "specialization": specialization,
        "academic_qualifications": academicQualifications,
        "medical_degree": medicalDegree,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "user": user.toJson(),
    };
}

class User {
    int id;
    String firstName;
    String fatherName;
    String lastName;
    String motherName;
    String gender;
    String city;
    String communicationNumber;
    String address;
    DateTime birthdate;
    String nationalNumber;
    dynamic passportNumber;
    String email;
    dynamic emailVerifiedAt;
    int roleId;
    dynamic ehrId;
    DateTime createdAt;
    DateTime updatedAt;

    User({
        required this.id,
        required this.firstName,
        required this.fatherName,
        required this.lastName,
        required this.motherName,
        required this.gender,
        required this.city,
        required this.communicationNumber,
        required this.address,
        required this.birthdate,
        required this.nationalNumber,
        this.passportNumber,
        required this.email,
        this.emailVerifiedAt,
        required this.roleId,
        this.ehrId,
        required this.createdAt,
        required this.updatedAt,
    });

    factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        firstName: json["first_name"],
        fatherName: json["father_name"],
        lastName: json["last_name"],
        motherName: json["mother_name"],
        gender: json["gender"],
        city: json["city"],
        communicationNumber: json["communication_number"],
        address: json["address"],
        birthdate: DateTime.parse(json["birthdate"]),
        nationalNumber: json["national_number"],
        passportNumber: json["passport_number"],
        email: json["email"],
        emailVerifiedAt: json["email_verified_at"],
        roleId: json["role_id"],
        ehrId: json["ehr_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "first_name": firstName,
        "father_name": fatherName,
        "last_name": lastName,
        "mother_name": motherName,
        "gender": gender,
        "city": city,
        "communication_number": communicationNumber,
        "address": address,
        "birthdate": "${birthdate.year.toString().padLeft(4, '0')}-${birthdate.month.toString().padLeft(2, '0')}-${birthdate.day.toString().padLeft(2, '0')}",
        "national_number": nationalNumber,
        "passport_number": passportNumber,
        "email": email,
        "email_verified_at": emailVerifiedAt,
        "role_id": roleId,
        "ehr_id": ehrId,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
    };
}
