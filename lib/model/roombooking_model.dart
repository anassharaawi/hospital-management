// To parse this JSON data, do
//
//     final roomBookingModel = roomBookingModelFromJson(jsonString);

import 'dart:convert';

List<RoomBookingModel> roomBookingModelFromJson(String str) => List<RoomBookingModel>.from(json.decode(str).map((x) => RoomBookingModel.fromJson(x)));

String roomBookingModelToJson(List<RoomBookingModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class RoomBookingModel {
    int id;
    DateTime entryDate;
    DateTime leaveDate;
    int roomId;
    int userId;
    DateTime createdAt;
    DateTime updatedAt;
    Rooms rooms;

    RoomBookingModel({
        required this.id,
        required this.entryDate,
        required this.leaveDate,
        required this.roomId,
        required this.userId,
        required this.createdAt,
        required this.updatedAt,
        required this.rooms,
    });

    factory RoomBookingModel.fromJson(Map<String, dynamic> json) => RoomBookingModel(
        id: json["id"],
        entryDate: DateTime.parse(json["entry_date"]),
        leaveDate: DateTime.parse(json["leave_date"]),
        roomId: json["room_id"],
        userId: json["user_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        rooms: Rooms.fromJson(json["rooms"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "entry_date": "${entryDate.year.toString().padLeft(4, '0')}-${entryDate.month.toString().padLeft(2, '0')}-${entryDate.day.toString().padLeft(2, '0')}",
        "leave_date": "${leaveDate.year.toString().padLeft(4, '0')}-${leaveDate.month.toString().padLeft(2, '0')}-${leaveDate.day.toString().padLeft(2, '0')}",
        "room_id": roomId,
        "user_id": userId,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "rooms": rooms.toJson(),
    };
}

class Rooms {
    int id;
    String degree;
    int beds;
    String price;
    String number;
    DateTime createdAt;
    DateTime updatedAt;

    Rooms({
        required this.id,
        required this.degree,
        required this.beds,
        required this.price,
        required this.number,
        required this.createdAt,
        required this.updatedAt,
    });

    factory Rooms.fromJson(Map<String, dynamic> json) => Rooms(
        id: json["id"],
        degree: json["degree"],
        beds: json["beds"],
        price: json["price"],
        number: json["number"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "degree": degree,
        "beds": beds,
        "price": price,
        "number": number,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
    };
}
