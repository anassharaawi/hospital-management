import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../view/Payments.dart';
import '../view/ambulance.dart';
import '../view/appointmentsdoctor_screen.dart';
import '../view/appointmentsoptions_scrren.dart';
import '../view/appoitmentsurgery_screen.dart';
import '../view/patient_screen.dart';
import '../view/ehr_screen.dart';
import '../view/prescriptions_screen.dart';
import '../view/room_booking.dart';

class HomeScreenController extends GetxController {
  int navBarValue = 1;
  Widget _currentScreen = const PatientScreen();
  get currentScreen => _currentScreen;

  void changeSelectedValue(int selectedValue) {
    navBarValue = selectedValue;
    switch (selectedValue) {
      case 0:
        _currentScreen = const EhrScreen();

        break;

      case 1:
        _currentScreen = const PatientScreen();
        break;

      case 2:
        // _currentScreen = const AppointmentdoctorScreen();
        _currentScreen = const AppointmentsOptions();
        break;
    }
    update();
  }
}
