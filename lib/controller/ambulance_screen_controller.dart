import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import '../services/api_service.dart';
import '../utlis/function.dart';

class ambulanceController extends GetxController {
  TextEditingController phonenumberController = TextEditingController();
  TextEditingController discriptionController = TextEditingController();
  late Position Location;
  // final formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  Future<void> onInit() async {
    super.onInit();
  phonenumberController = TextEditingController();
  discriptionController = TextEditingController();
    await getLocation();
  }

  getLocation() async {
    await Geolocator.checkPermission();
    await Geolocator.requestPermission();

    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.low);
    Location = position;
    // print(position);
    // print(Location);
    _isLoading = false;
  }

  set loading(bool val) {
    _isLoading = val;

    update();
  }

  get isLoading => _isLoading;

  Future<void> submit() async {
    loading = true;
    // if (formKey.currentState!.validate()) {
    final ApiService apiService = ApiService();
// try{
    var response = await apiService.ambulanceApi(
        phonenumber: phonenumberController.text,
        description: discriptionController.text,
        location: Location);
    if (response) {
      print("afasdasda");
      print(phonenumberController);
      print(discriptionController);
      showSnackbar("Done  ", 'create Ambulance', backgroundColor: Colors.green);
      Get.back();
    } else {
      print("anas");
      showSnackbar("Error  ", 'didnt create Ambulance',
          backgroundColor: Colors.red);
    }
    // } catch (e) {
    //   showSnackbar("some thing went wrong", "please try again");
    // }

    loading = false;
    update();
  }
}
