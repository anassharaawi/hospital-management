import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../model/prescription_model.dart';
import '../services/api_service.dart';
import '../utlis/function.dart';

class PrescriptionsController extends GetxController {
  late List<PrescriptionsModel> prescriptionsModel;
  bool _isLoading = false;

  @override
  void onInit() {
    getPrescriptionsductorShow();
    super.onInit();
  }

  get isLoading => _isLoading;

  set loading(bool val) {
    _isLoading = val;
    update();
  }

  Future<void> getPrescriptionsductorShow() async {
    loading = true;
    prescriptionsModel =  prescriptionsModelFromJson(json.encode(
     [
    {
        "id": 1,
        "date": "2023-06-22",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec finibus mauris, et aliquam nibh. Donec quis elit sit amet velit mattis imperdiet sit amet at metus.",
        "created_at": "2023-06-21T21:43:36.000000Z",
        "updated_at": "2023-06-21T21:43:36.000000Z",
        "doctor_first_name": "Monzer",
        "doctor_last_name": "Mohammad",
        "medical_analysis": [
            {
                "id": 1,
                "name": "Vitamin B12",
                "availability": "1",
                "price": 100000,
                "created_at": "2023-06-21T21:05:02.000000Z",
                "updated_at": "2023-06-21T21:05:12.000000Z",
                "pivot": {
                    "medical_prescription_id": 1,
                    "medical_analysis_id": 1
                }
            },
            {
                "id": 2,
                "name": "Vitamin D",
                "availability": "1",
                "price": 35000,
                "created_at": "2023-06-21T21:05:35.000000Z",
                "updated_at": "2023-06-21T21:05:35.000000Z",
                "pivot": {
                    "medical_prescription_id": 1,
                    "medical_analysis_id": 2
                }
            },
            {
                "id": 5,
                "name": "Hemoglobin",
                "availability": "1",
                "price": 95000,
                "created_at": "2023-06-21T21:08:19.000000Z",
                "updated_at": "2023-06-21T21:08:19.000000Z",
                "pivot": {
                    "medical_prescription_id": 1,
                    "medical_analysis_id": 5
                }
            }
        ],
        "radiograph": [
            {
                "id": 1,
                "name": "Chest X-ray",
                "availability": "1",
                "price": 10000,
                "created_at": "2023-06-21T21:19:35.000000Z",
                "updated_at": "2023-06-21T21:22:41.000000Z",
                "pivot": {
                    "medical_prescription_id": 1,
                    "radiograph_id": 1
                }
            },
            {
                "id": 3,
                "name": "Chest CAT",
                "availability": "1",
                "price": 150000,
                "created_at": "2023-06-21T21:25:24.000000Z",
                "updated_at": "2023-06-21T21:25:24.000000Z",
                "pivot": {
                    "medical_prescription_id": 1,
                    "radiograph_id": 3
                }
            }
        ],
        "medicine": [
            {
                "id": 1,
                "name": "Cetrizin",
                "category": "anti-hestamin",
                "sell_price": 1500,
                "quantity": 50,
                "company": "Avenzore",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec finibus mauris, et aliquam nibh.",
                "created_at": "2023-06-21T21:09:06.000000Z",
                "updated_at": "2023-06-21T21:09:06.000000Z",
                "pivot": {
                    "medical_prescription_id": 1,
                    "medicine_id": 1,
                    "dosage": "2"
                }
            }
        ]
    },
    {
        "id": 2,
        "date": "2023-06-21",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec finibus mauris, et aliquam nibh.",
        "created_at": "2023-06-21T21:49:14.000000Z",
        "updated_at": "2023-06-21T21:49:14.000000Z",
        "doctor_first_name": "Nasser",
        "doctor_last_name": "Sakkal",
        "medical_analysis": [],
        "radiograph": [],
        "medicine": [
            {
                "id": 3,
                "name": "Panadol",
                "category": "pain-killer",
                "sell_price": 2000,
                "quantity": 70,
                "company": "Unifarma",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec finibus mauris, et aliquam nibh.",
                "created_at": "2023-06-21T21:11:05.000000Z",
                "updated_at": "2023-06-21T21:11:05.000000Z",
                "pivot": {
                    "medical_prescription_id": 2,
                    "medicine_id": 3,
                    "dosage": "5"
                }
            },
            {
                "id": 2,
                "name": "Fexofenadine",
                "category": "anti-hestamin",
                "sell_price": 3000,
                "quantity": 43,
                "company": "Rama Farma",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec finibus mauris, et aliquam nibh.",
                "created_at": "2023-06-21T21:10:22.000000Z",
                "updated_at": "2023-06-21T21:10:22.000000Z",
                "pivot": {
                    "medical_prescription_id": 2,
                    "medicine_id": 2,
                    "dosage": "7.2"
                }
            },
            {
                "id": 4,
                "name": "Toplexil",
                "category": "cough medicine",
                "sell_price": 6700,
                "quantity": 42,
                "company": "avenzore",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec finibus mauris, et aliquam nibh.",
                "created_at": "2023-06-21T21:13:06.000000Z",
                "updated_at": "2023-06-21T21:13:06.000000Z",
                "pivot": {
                    "medical_prescription_id": 2,
                    "medicine_id": 4,
                    "dosage": "10"
                }
            }
        ]
    }
]
)); 
    final ApiService apiService = ApiService();
    try {
      final http.Response? response = await apiService.prescriptionsShowApi();
      if (response != null) {
        debugPrint("Prescription show api code ${response.statusCode}");
        debugPrint(response.body);

        if (response.statusCode == 200) {
          prescriptionsModel = prescriptionsModelFromJson(response.body);
        } else {
          showSnackbar("error ${response.statusCode}", response.body);
        }
      } else {
        showSnackbar("no response", "check your internet connection");
      }
    } catch (e) {
      showSnackbar("some thing went wrong", "please try again");
    }

    loading = false;
  }
}
