import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:osama_test/model/Ehr_model.dart';

import '../model/Patient_Model.dart';
import '../services/api_service.dart';
import '../utlis/function.dart';

class PatientController extends GetxController {
  late PatientDetailsModel patientDetailsModel;
  bool _isLoading = false;

  @override
  void onInit() {
    getPatientShow();
    super.onInit();
  }

  get isLoading => _isLoading;

  set loading(bool val) {
    _isLoading = val;
    update();
  }

  Future<void> getPatientShow() async {
    loading = true;

    final ApiService apiService = ApiService();
    try {
      final http.Response? response = await apiService.pationtshowApi(userId: 1);
      if (response != null) {
        debugPrint("Patient show api code ${response.statusCode}");
        debugPrint(response.body);

        if (response.statusCode == 200) {
          patientDetailsModel = patientDetailsModelFromJson(response.body);

        } else {
          showSnackbar("error ${response.statusCode}", response.body);
        }
      } else {
        showSnackbar("no response", "check your internet connection");
      }
    } catch (e) {
      showSnackbar("some thing went wrong", "please try again");
    }

    loading = false;
  }

  //         Get.offAllNamed('/home');
  //       } else {
  //         Get.snackbar(
  //           'Sorry , ',
  //           'sorry',
  //           snackPosition: SnackPosition.TOP,
  //           backgroundColor: Colors.deepOrangeAccent,
  //         );
  //       }
  //     } catch (e) {
  //       print(e.toString());
  //     }
  //   }
}
