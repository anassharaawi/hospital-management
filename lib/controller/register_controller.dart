// import 'dart:convert';

// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:http/http.dart' as http;

// import '../global/statics.dart';


// class RegisterController extends GetxController {
//   final TextEditingController firstNameController = TextEditingController();
//   final TextEditingController lastNameController = TextEditingController();
//   final TextEditingController emailController = TextEditingController();
//   final TextEditingController phoneNumberController = TextEditingController();
//   final TextEditingController passwordController = TextEditingController();
//   final TextEditingController passwordConfirmationController =
//   TextEditingController();
//   final registerFormKey = GlobalKey<FormState>();

//   Future<void> submit() async {
//     if (registerFormKey.currentState!.validate()) {
//       registerFormKey.currentState?.save();

//       Map<String, String> headers = {
//         'Content-Type': 'application/json',
//         'Accept': 'application/json',
//       };

//       Map<String, String> body = {
//         'name':
//         '${firstNameController.text} ${lastNameController.text}',
//         'username': emailController.text,
//         'password': passwordController.text,
//       };

//       http.Response response = await http.post(
//           Uri.http(Statics.host , '/api/v1/users/save/'),
//           headers: headers,
//           body: jsonEncode(body));

//       if(response.statusCode == 201){
//         Get.snackbar(
//           'Welcome',
//           'registered successfully',
//           snackPosition: SnackPosition.TOP,
//           backgroundColor: Colors.greenAccent,
//         );
//         await Future.delayed(const Duration(seconds: 3));
//         Get.offNamed('/login');
//       }else{
//         Get.snackbar(
//           'error code : ${response.statusCode}',
//           'error message : ${response.body}',
//           snackPosition: SnackPosition.TOP,
//           backgroundColor: Colors.deepOrangeAccent,
//         );
//       }
//     }
//   }
// }
