import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:osama_test/services/http_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../global/statics.dart';
import '../model/login_model.dart';
import '../services/api_service.dart';
import '../utlis/function.dart';

class LoginController extends GetxController {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  bool _isLoading = false;

  set loading(bool val) {
    _isLoading = val;
    print(val);
    update();
  }

  get isLoading => _isLoading;

  Future<void> submit() async {
    loading = true;
    if (formKey.currentState!.validate()) {
      final ApiService apiService = ApiService();
      try {
        final http.Response? response = await apiService.loginApi(
            email: emailController.text, password: passwordController.text);
        if (response != null) {
          debugPrint("login api code ${response.statusCode}");
          debugPrint(response.body);
          var parsedBody = json.decode(response.body);

          if (response.statusCode == 200 && parsedBody['error'] == null) {
            var sharedPreferences = SharedPreferences.getInstance();

            final loginModel = loginModelFromJson(response.body);
            HttpService.headers['Authorization'] =
                'Bearer ${parsedBody['token']}';
            // sharedPreferences.setString("TOKEN", parsedBody['token']);
            // sharedPreferences.setString("ROLE", parsedBody['token']);
            if (loginModel.roles.first == "Doctor") {
              Get.offAllNamed("/AppointmentdoctorpationtScreen");
            } else {
              Get.offAllNamed("/home");
              print("hihihihihhihihi");
            }
          } else {
            showSnackbar("error ${response.statusCode}", response.body);
          }
        } else {
          showSnackbar("no response", "check your internet connection");
        }
      } catch (e) {
        showSnackbar("some thing went wrong", "please try again");
      }
    }
    loading = false;
  }
}
