import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import '../model/roombooking_model.dart';
import '../services/api_service.dart';
import '../utlis/function.dart';

class RoomBookingController extends GetxController {
  late List<RoomBookingModel> roomBookingModel;
  bool _isLoading = false;

  @override
  void onInit() {
    getRoomBookingShow();
    super.onInit();
  }

  get isLoading => _isLoading;

  set loading(bool val) {
    _isLoading = val;
    update();
  }

  Future<void> getRoomBookingShow() async {
    loading = true;

    final ApiService apiService = ApiService();
    try {
      final http.Response? response =
          await apiService.roomBookingShowApi(userId: 1);
      if (response != null) {
        debugPrint("Room Booking show api code ${response.statusCode}");
        debugPrint(response.body);

        if (response.statusCode == 200) {
          roomBookingModel = roomBookingModelFromJson(response.body);
          
        } else {
          showSnackbar("error ${response.statusCode}", response.body);
        }
      } else {
        showSnackbar("no response", "check your internet connection");
      }
    } catch (e) {
      print(e);
      showSnackbar("some thing went wrong", "please try again");
    }

    loading = false;
  }
  //  Future<void> submit() async {
  //     Map<String, String> headers = {
  //       'Accept': 'application/json',
  //     };

  //     try {
  //       http.Response response = await http.get(
  //           Uri.http(Statics.host, '/api/ehrs/1'),
  //           headers: headers, );
  //       if (response.statusCode == 200) {

  //         await Future.delayed(const Duration(seconds: 2));
  //         Get.offAllNamed('/home');
  //       } else {
  //         Get.snackbar(
  //           'Sorry , ',
  //           'sorry',
  //           snackPosition: SnackPosition.TOP,
  //           backgroundColor: Colors.deepOrangeAccent,
  //         );
  //       }
  //     } catch (e) {
  //       print(e.toString());
  //     }
  //   }
}
