import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:osama_test/model/Ehr_model.dart';
import 'dart:io';
import 'package:flutter/material.dart';
// import 'package:flutter_file_downloader/flutter_file_downloader.dart';
// import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import '../model/ehr_asset_model.dart';
import '../services/api_service.dart';
import '../utlis/function.dart';

class EhrController extends GetxController {
  late EhrModel ehrModel;
  late EhrAssetModel ehrAssetModel;
  bool _isLoading = false;

  @override
  void onInit() {
    getEhrShow();
    super.onInit();
  }

  get isLoading => _isLoading;

  set loading(bool val) {
    _isLoading = val;
    update();
  }

  Future<void> getEhrShow() async {
    loading = true;

    final ApiService apiService = ApiService();
    try {
      final http.Response? response = await apiService.ehrShowApi(userId: 1);
      if (response != null) {
        debugPrint("ehr show api code ${response.statusCode}");
        debugPrint(response.body);

        if (response.statusCode == 200) {
          ehrModel = edrModelFromJson(response.body);
        } else {
          showSnackbar("error ${response.statusCode}", response.body);
        }
      } else {
        showSnackbar("no response", "check your internet connection");
      }
    } catch (e) {
      showSnackbar("some thing went wrong", "please try again");
    }

    loading = false;
  }

  Future<void> getAssetEhrShow() async {
    loading = true;

    final ApiService apiService = ApiService();
    try {
      final http.Response? response =
          await apiService.ehrAssetShowApi(userId: 7);
      if (response != null) {
        debugPrint("ehr show api code ${response.statusCode}");
        debugPrint(response.body);

        if (response.statusCode == 200) {
          final ehrAssetModel = ehrAssetModelFromJson(response.body);
          // _openPdfFile();
        } else {
          showSnackbar("error ${response.statusCode}", response.body);
        }
      } else {
        showSnackbar("no response", "check your internet connection");
      }
    } catch (e) {
      showSnackbar("some thing went wrong", "please try again");
    }

    loading = false;
  }

  // Future<String> _downloadFile(String url, String savedDir) async {
  // final taskId = await FlutterDownloader.enqueue(
  //   url: url,
  //   savedDir: savedDir,
  //   showNotification:
  //       true, // Show download progress in the notification bar (optional)
  //   openFileFromNotification:
  //       true, // Open the downloaded file when tapping the notification (optional)

  // );

  //   FileDownloader.downloadFile(
  //       url: url,
  //       name: savedDir,
  //       onProgress: (fileName, progress) => {},
  //       onDownloadError: (_) {
  //         return 'error';
  //       },
  //       onDownloadCompleted: (path) {
  //         final File file = File(path);
  //         return path;
  //         //This will be the path of the downloaded file
  //       });
  //   return ' ';
  // }

  // void _openPdfFile() async {
  //   final directory = await getExternalStorageDirectory();

  //   // Specify the directory where the file will be saved
  //   final savedDir = directory!.path;

  //   final savedPath =
  //       await _downloadFile(ehrAssetModel.attachments.first.src, savedDir);

  //   // Open the downloaded PDF file
  //   await OpenFile.open(savedPath);
  // }
  //  Future<void> submit() async {
  //     Map<String, String> headers = {
  //       'Accept': 'application/json',
  //     };

  //     try {
  //       http.Response response = await http.get(
  //           Uri.http(Statics.host, '/api/ehrs/1'),
  //           headers: headers, );
  //       if (response.statusCode == 200) {

  //         await Future.delayed(const Duration(seconds: 2));
  //         Get.offAllNamed('/home');
  //       } else {
  //         Get.snackbar(
  //           'Sorry , ',
  //           'sorry',
  //           snackPosition: SnackPosition.TOP,
  //           backgroundColor: Colors.deepOrangeAccent,
  //         );
  //       }
  //     } catch (e) {
  //       print(e.toString());
  //     }
  //   }
}

