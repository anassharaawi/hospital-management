import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;

import 'http_service.dart';

class ApiService {
  late final HttpService httpService;

  ApiService() {
    httpService = HttpService();
  }

  Future ambulanceApi(
      {required Position location,
      required String phonenumber,
      required String description}) async {
    var request = http.MultipartRequest(
        'POST', Uri.parse('${httpService.baseUrl}ambulances'));
    request.fields.addAll({
      'description': description.toString(),
      'phone_number': phonenumber.toString(),
      'lat': location.latitude.toString(),
      'lng': location.longitude.toString(),
    });

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print("annnnas" + "$request");
      print(description);
      print(phonenumber);

      print("annnnas" + "$request");

      print(await response.stream.bytesToString());
    } else {
      print(request.fields);
      print("anas" + "$request.");
      print(response.reasonPhrase);
    }
  }

  Future loginApi({required String email, required String password}) async {
    Response? response;
    try {
      response = await httpService.post(
        "patients/login",
        {
          "email": email,
          "password": password,
        },
      );
    } catch (e) {
      debugPrint("LOGIN API ERROR : $e");
    }
    return response;
  }

  Future ehrShowApi({required int userId}) async {
    Response? response;
    try {
      response = await httpService.get(
        "showPatientEhr",
      );
    } catch (e) {
      debugPrint("ehr show API ERROR : $e");
    }
    return response;
  }

  Future ehrAssetShowApi({required int userId}) async {
    Response? response;
    try {
      response = await httpService.get(
        "ehr-assets/$userId",
      );
    } catch (e) {
      debugPrint("ehr show API ERROR : $e");
    }
    return response;
  }

  Future getappointmentsdoctorpationtShowShowApi({required int userId}) async {
    Response? response;
    try {
      response = await httpService.get(
        "doctor-appointments",
      );
    } catch (e) {
      debugPrint("appointments doctor pationt show API ERROR : $e");
    }
    return response;
  }

  Future appointmentsDoctorSurgeryShowApi({required int userId}) async {
    Response? response;
    try {
      response = await httpService.get(
        "doctor-operations",
      );
    } catch (e) {
      debugPrint("appointments doctor surgery show API ERROR : $e");
    }
    return response;
  }

  Future appointmentductorShowApi({required int userId}) async {
    Response? response;
    try {
      response = await httpService.get(
        "showPatientAppointment",
      );
    } catch (e) {
      debugPrint("appointments doctor show API ERROR : $e");
    }
    return response;
  }

  Future appointmentSurgeryShowApi({required int userId}) async {
    Response? response;
    try {
      response = await httpService.get(
        "showOperations",
      );
    } catch (e) {
      debugPrint("appointments Surgery show API ERROR : $e");
    }
    return response;
  }

  Future roomBookingShowApi({required int userId}) async {
    Response? response;
    try {
      response = await httpService.get(
        "showBookings",
      );
    } catch (e) {
      debugPrint("Room Booking show API ERROR : $e");
    }
    return response;
  }

  Future pationtshowApi({required int userId}) async {
    Response? response;
    try {
      response = await httpService.get(
        "showPatientDetails",
      );
    } catch (e) {
      debugPrint("Pationt show API ERROR : $e");
    }
    return response;
  }

  Future prescriptionsShowApi() async {
    Response? response;
    try {
      response = await httpService.get(
        "showMedicalPrescriptions",
      );
    } catch (e) {
      debugPrint("appointments show API ERROR : $e");
    }
    return response;
  }
}
// showPatientEhr/