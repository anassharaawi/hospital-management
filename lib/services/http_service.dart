import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HttpService {
  // static String baseUrl = "http://127.0.0.1:8000/api/";
   String baseUrl = "http://192.168.43.128:8000/api/";
  // static String baseUrl = "http://192.168.1.4:8000/api/";

  static final headers = {
    "Content-Type": "application/json",
    "Authorization": "Bearer"
  };

  Future<http.Response?> get(String endpoint) async {
    http.Response? response;
    try {
      response =
          await http.get(Uri.parse(baseUrl + endpoint), headers: headers);
    } catch (e) {
      debugPrint("get req error : $e");
    }
    return response;
  }

  Future<dynamic> post(String endpoint, dynamic data) async {
    http.Response? response;
    try {
      response = await http.post(Uri.parse(baseUrl + endpoint),
          headers: headers, body: json.encode(data));
    } catch (e) {
      debugPrint("psot req error : $e");
    }
    return response;
  }
}
