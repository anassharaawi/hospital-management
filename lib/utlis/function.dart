import 'package:flutter/material.dart';
import 'package:get/get.dart';

void showSnackbar(String title, String massage,
    {Color? colorText, Color? backgroundColor}) {
  if (!Get.isSnackbarOpen) {
    Get.snackbar(title, massage,
        snackPosition: SnackPosition.BOTTOM,
        colorText: colorText,
        backgroundColor: backgroundColor,
        margin: const EdgeInsets.all(20));
  }
}
