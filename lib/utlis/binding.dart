import 'package:get/get.dart';
import 'package:osama_test/view/ambulance.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../controller/home_screen_controller.dart';
import '../controller/login_controller.dart';
import '../view/splash_screen.dart';

class Binding extends Bindings {
  @override
  void dependencies() {
    final shared = SharedPreferences.getInstance();
    Get.lazyPut(() => LoginController());
    // Get.lazyPut(() => RegisterController());
    Get.lazyPut(() => HomeScreenController());
    Get.lazyPut(() => AmbulanceScreen());
    Get.put(() => shared);
    Get.lazyPut(() => const Splashscreen());

    Get.lazyPut(() => shared);
  }
}
