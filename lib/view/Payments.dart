import 'package:flutter/material.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:toast/toast.dart';

class Payment extends StatefulWidget {
  const Payment({super.key});

  @override
  State<Payment> createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  late Razorpay razorpay;
  TextEditingController textEditingController = TextEditingController();
  void initSatats() {

     

    razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, handlerPaymentSuccess);
    razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, handlerPaymentError);
    razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, handlerPaymentWallet);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    razorpay.clear();
  }

  void openCheckout() {
     razorpay = Razorpay();
    var options = {
      "key": "rzp_test_GCJFYYlJK3SI7n",
      "amount": num.parse(textEditingController.text) * 100,
      "name": "Sample App",
      "description": "Payment for the somme random product",
      "Profill": {"contact": "12345678", "email": "anassharaawi@gmail.com"},
      "external": {
        "Wallets": ["paytm"]
      }
    };
    try {
      razorpay.open(options);
    } catch (e) {
      print(e.toString());
    }
  }

  void handlerPaymentSuccess() {
    print("payment success");
    Toast.show("payment success");
  }

  void handlerPaymentError() {
    print("payment error");
    Toast.show("payment error");
  }

  void handlerPaymentWallet() {
    print("payment wallet");
        Toast.show("payment wallet");

    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff24cbc8),
        title: const Text("Razor pay"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(22.0),
        child: Column(children: [
          TextField(
            controller: textEditingController,
            decoration: const InputDecoration(hintText: "amount to pay"),
          ),
          const SizedBox(
            height: 12,
          ),
          ElevatedButton(
            child: const Text("data", style: TextStyle(color: Colors.black)),
            onPressed: ()  {
              openCheckout();
            },
          )
        ]),
      ),
    );
  }
}
