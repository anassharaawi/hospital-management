// import 'package:flutter/gestures.dart';
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';

// import '../../../controller/register_controller.dart';
// import '../../../global/theme/app_colors/app_light_colors.dart';
// import '../../../global/widgets/customized_form_field.dart';

// class RegisterScreen extends GetView<RegisterController> {
//   const RegisterScreen({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: SafeArea(
//         child: Padding(
//           padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
//           child: SingleChildScrollView(
//             child: SizedBox(
//               height: Get.height * .9,
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   const Text(
//                     'Get Started',
//                     style: TextStyle(
//                       fontSize: 26.0,
//                       fontWeight: FontWeight.bold,
//                       color: AppLightColors.primaryColor,
//                       // fontFamily: 'roboto',
//                     ),
//                   ),
//                   Form(
//                     key: controller.registerFormKey,
//                     child: SizedBox(
//                       height: Get.height * .6,
//                       child: Column(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           // Name
//                           CustomizedFormField(
//                             textInputType: TextInputType.name,
//                             prefixIcon: Icons.person,
//                             labelText: 'First Name',
//                             textEditingController:
//                             controller.firstNameController,
//                             validate: (value) {
//                               if (value.trim().isEmpty) return 'Required';
//                             },
//                           ),
//                           CustomizedFormField(
//                             textInputType: TextInputType.name,
//                             prefixIcon: Icons.person,
//                             labelText: 'Last Name',
//                             textEditingController:
//                             controller.lastNameController,
//                             validate: (value) {
//                               if (value.trim().isEmpty) return 'Required';
//                             },
//                           ),
//                           // Email
//                           CustomizedFormField(
//                             textInputType: TextInputType.emailAddress,
//                             prefixIcon: Icons.email,
//                             labelText: 'Email Address',
//                             textEditingController: controller.emailController,
//                             validate: (value) {
//                               if (value.trim().isEmpty) return 'Required';
//                               if (!RegExp(
//                                   r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
//                                   .hasMatch(value)) {
//                                 return 'Enter a valid email';
//                               }
//                             },
//                           ),
//                           CustomizedFormField(
//                             textInputType: TextInputType.visiblePassword,
//                             prefixIcon: Icons.vpn_key_rounded,
//                             labelText: 'Password',
//                             textEditingController:
//                             controller.passwordController,
//                             obscureText: true,
//                             autocorrect: false,
//                             enableSuggestions: false,
//                             validate: (value) {
//                               if (value.trim().isEmpty) return 'Required';
//                               if (controller
//                                   .passwordConfirmationController.text !=
//                                   controller.passwordController.text) {
//                                 return 'Passwords don\'t match';
//                               }
//                             },
//                           ),
//                           // Password Confirmation
//                           CustomizedFormField(
//                             textInputType: TextInputType.visiblePassword,
//                             prefixIcon: Icons.vpn_key_rounded,
//                             labelText: 'Password Confirmation',
//                             textEditingController:
//                             controller.passwordConfirmationController,
//                             obscureText: true,
//                             autocorrect: false,
//                             enableSuggestions: false,
//                             validate: (value) {
//                               if (value.trim().isEmpty) return 'Required';
//                             },
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                   SizedBox(
//                     height: Get.height * .05,
//                     child: Row(
//                       children: [
//                         const Text('Already have an account?'),
//                         TextButton(
//                             onPressed: () => Get.toNamed('/login'),
//                             child: const Text('Login'))
//                       ],
//                     ),
//                   ),
//                   SizedBox(
//                     width: double.infinity,
//                     height: 56.0,
//                     child: TextButton(
//                       style: TextButton.styleFrom(
//                         backgroundColor: AppLightColors.primaryColor,
//                         shape: RoundedRectangleBorder(
//                           borderRadius: BorderRadius.circular(10.0),
//                         ),
//                       ),
//                       onPressed: () async {
//                         await controller.submit();
//                       },
//                       child: const Text(
//                         "Register",
//                         style: TextStyle(
//                           color: Colors.white,
//                           fontFamily: 'robot',
//                           fontWeight: FontWeight.bold,
//                           letterSpacing: 1.1,
//                           fontSize: 18.0,
//                         ),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
