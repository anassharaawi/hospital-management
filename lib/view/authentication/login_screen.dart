import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/login_controller.dart';
import '../../global/theme/app_colors/app_light_colors.dart';
import '../../global/widgets/customized_form_field.dart';
import '../ambulance.dart';

class LoginScreen extends GetView<LoginController> {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.red,
          child: Image.asset("assets/images/ambulance.png",width: 33,height: 33,),
          onPressed: () {
            Get.to(const AmbulanceScreen());
          }),
      // floatingActionButton: GestureDetector(
      //   onTap: () => Get.to('/ambulance'),
      //   child:  Column(children: [
      //     Icon(
      //       Icons.sos_outlined,
      //       color: Colors.red,
      //       size: 40,
      //     ),
      //     Image.asset("assets/images/ambulance.png",),
      //   ]),
      // ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: Get.height * .1),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Welcome\nBack',
                      style: Theme.of(context).textTheme.displayLarge,
                    ),
                  ],
                ),
                SizedBox(height: Get.height * .1),
                Form(
                    key: controller.formKey,
                    child: Column(
                      children: [
                        // Email
                        CustomizedFormField(
                          textInputType: TextInputType.emailAddress,
                          prefixIcon: Icons.email,
                          labelText: 'Email Address',
                          textEditingController: controller.emailController,
                          validate: (value) {
                            if (value.trim().isEmpty) return 'Required';
                            if (!RegExp(
                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                .hasMatch(value)) {
                              return 'Enter a valid email';
                            }
                          },
                        ),

                        const SizedBox(height: 30.0),

                        // Password
                        CustomizedFormField(
                          textInputType: TextInputType.visiblePassword,
                          obscureText: true,
                          autocorrect: false,
                          enableSuggestions: false,
                          prefixIcon: Icons.vpn_key_rounded,
                          labelText: 'Password',
                          textEditingController: controller.passwordController,
                          validate: (value) {
                            if (value.trim().isEmpty) return 'Required';
                            if (value.length < 6) {
                              return 'Password must be at least 6 characters';
                            }
                          },
                        ),
                      ],
                    )),
                // SizedBox(height: Get.height * 0.05),
                // Row(
                //   children: [
                //     const Text('Don\'t have an account yet?'),
                //     TextButton(
                //       onPressed: () {
                //         Get.toNamed('/register');
                //       },
                //       child: const Text('Register'),
                //     ),
                //   ],
                // ),
                SizedBox(height: Get.height * .075),
                Center(
                  child: SizedBox(
                    width: Get.width * 0.8,
                    height: Get.height * .07,
                    child: controller.isLoading
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : TextButton(
                            style: TextButton.styleFrom(
                              backgroundColor: AppLightColors.primaryColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                            ),
                            onPressed: () {
                              controller.submit();
                            },
                            child: const Text(
                              "Login",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'opensans',
                                fontWeight: FontWeight.bold,
                                letterSpacing: 1.1,
                                fontSize: 18.0,
                              ),
                            ),
                          ),
                  ),
                ),
               
              ],
            ),
          ),
        ),
      ),
    );
  }
}
