import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:osama_test/controller/patient_screen_controler.dart';

class PatientScreen extends StatelessWidget {
  const PatientScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        body: SafeArea(
            child: GetBuilder<PatientController>(
          init: PatientController(),
          builder: (PatientController controller) => controller.isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : SingleChildScrollView(
                  padding: const EdgeInsets.all(20),
                  child: Stack(children: [
                    Container(
                      clipBehavior: Clip.hardEdge,
                      width: double.infinity,
                      margin: const EdgeInsets.only(top: 70),
                      padding: const EdgeInsets.fromLTRB(0, 80, 0, 0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(30)),
                      child: Column(
                        children: [
                          Text(
                            "${controller.patientDetailsModel.firstName} ${controller.patientDetailsModel.lastName}",
                            style: TextStyle(
                              fontSize: 26,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Material(
                            child: ListTile(
                              title: Text(
                                "Father Name",
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                  controller.patientDetailsModel.fatherName),
                              // isThreeLine: true,
                              onTap: () {},
                            ),
                          ),
                          Material(
                            child: ListTile(
                              title: Text(
                                "Mother Name",
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                  controller.patientDetailsModel.motherName),
                              // isThreeLine: true,
                              onTap: () {},
                            ),
                          ),
                          Material(
                            child: ListTile(
                              title: Text(
                                "Gender",
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle:
                                  Text(controller.patientDetailsModel.gender),
                              // isThreeLine: true,
                              onTap: () {},
                            ),
                          ),
                          Material(
                            child: ListTile(
                              title: Text(
                                "city",
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle:
                                  Text(controller.patientDetailsModel.city),
                              // isThreeLine: true,
                              onTap: () {},
                            ),
                          ),
                          Material(
                            child: ListTile(
                              title: Text(
                                "communication number",
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(controller
                                  .patientDetailsModel.communicationNumber),
                              // isThreeLine: true,
                              onTap: () {},
                            ),
                          ),
                          Material(
                            child: ListTile(
                              title: Text(
                                "address",
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle:
                                  Text(controller.patientDetailsModel.address),
                              // isThreeLine: true,
                              onTap: () {},
                            ),
                          ),
                          Material(
                            child: ListTile(
                              title: Text(
                                "birthdate",
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(controller
                                  .patientDetailsModel.birthdate.timeZoneName),
                              // isThreeLine: true,
                              onTap: () {},
                            ),
                          ),
                          Material(
                            child: ListTile(
                              title: Text(
                                "national_number",
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(controller
                                  .patientDetailsModel.nationalNumber),
                              // isThreeLine: true,
                              onTap: () {},
                            ),
                          ),
                          // Material(
                          //   child: ListTile(
                          //     title: Text(
                          //       "passport_number",
                          //       style: TextStyle(
                          //           color: Colors.blueGrey,
                          //           fontWeight: FontWeight.bold),
                          //     ),
                          //     subtitle: Text(controller
                          //         .patientDetailsModel.passportNumber),
                          //     // isThreeLine: true,
                          //     onTap: () {},
                          //   ),
                          // ),
                          Material(
                            child: ListTile(
                              title: Text(
                                "email",
                                style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle:
                                  Text(controller.patientDetailsModel.email),
                              // isThreeLine: true,
                              onTap: () {},
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Positioned(
                      // top: 0,
                      right: 0,
                      left: 0,
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 70,
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: CircleAvatar(
                            backgroundColor: Colors.blueGrey,
                            radius: 70,
                            child: Icon(
                              Icons.person,
                              color: Colors.white,
                              size: 70,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ]),
                ),
        )));

    //     initZoom: 11,
    //     minZoomLevel: 5,
    //     maxZoomLevel: 16,
    //     trackMyPosition: true,
    //     searchBarBackgroundColor: Colors.amber,
    //     mapLanguage: 'ar',
    //     onError: (e) => print(e),
    //     onPicked: (pickedData) {
    //       print(pickedData.latLong.latitude);
    //       print(pickedData.latLong.longitude);
    //     }),
  }
// _pickLocation() async {
//   Navigator.push(
//     Get.context!,
//     MaterialPageRoute(
//       builder: (context) {
//         return MapLocationPicker(
//           apiKey: "AIzaSyBVPOeHY93rhxShwra3MP6j5Q8biFZK5tA",
//           canPopOnNextButtonTaped: true,
//           onNext: (GeocodingResult? result) async {
//             print('lat\t ${result?.geometry.location.lat}');
//             print('lng\t ${result?.geometry.location.lng}');
//             // if (result != null && isElctronic == false) {
//             //   await controller.checkOut(false, result.geometry.location.lat,
//             //       result.geometry.location.lng);
//             // } else if (result != null && isElctronic == true) {
//             //   controller.lat = result.geometry.location.lat;
//             //   controller.lng = result.geometry.location.lng;
//             //   controller.checkOut(true, result.geometry.location.lat,
//             //       result.geometry.location.lng);
//             // }
//           },
//         );
//       },
//     ),
//   );
// }
}
// body:FlutterLocationPicker(
