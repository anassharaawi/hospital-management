import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:osama_test/controller/ehr_screen_controller.dart';
import 'package:osama_test/global/widgets/titel_container.dart';
import '../global/theme/app_colors/app_light_colors.dart';
import '../global/widgets/drawerwidget.dart';

class EhrScreen extends StatefulWidget {
  const EhrScreen({Key? key}) : super(key: key);

  @override
  State<EhrScreen> createState() => SlideAnimation3();
}

class SlideAnimation3 extends State<EhrScreen> {
  bool gettingData = false;

  @override
  Widget build(BuildContext context) {
    double _w = MediaQuery.of(context).size.width;
    return Scaffold(
        drawer: DrwerWidget(),
        appBar: AppBar(
          backgroundColor: AppLightColors.primaryColor,
          title: Text("EhrScreen"),
          centerTitle: true,
          systemOverlayStyle: SystemUiOverlayStyle.light,
        ),
        body: GetBuilder<EhrController>(
          init: EhrController(),
          builder: (EhrController controller) => controller.isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 50, horizontal: 20),
                  children: [
                    TitleContainer(
                        title: 'Food Allergies',
                        content: controller.ehrModel.foodAllergies),
                    TitleContainer(
                        title: 'Medicine Allergies',
                        content: controller.ehrModel.medicineAllergies),
                    TitleContainer(
                        title: 'Heart Disease',
                        content: controller.ehrModel.heartDisease),
                    TitleContainer(
                        title: 'Blood Pressure',
                        content: controller.ehrModel.bloodPressure),
                    TitleContainer(
                        title: 'Diabetic',
                        content: controller.ehrModel.diabetic),
                    TitleContainer(
                        title: 'Bleed Tendency',
                        content: controller.ehrModel.bleedTendency),
                    TitleContainer(
                        title: 'Surgery', content: controller.ehrModel.surgery),
                    TitleContainer(
                        title: 'Blood Group',
                        content: controller.ehrModel.bloodGroup),
                    TitleContainer(
                        title: 'Accident',
                        content: controller.ehrModel.accident),
                    TitleContainer(
                        title: 'Familly Medical History ',
                        content: controller.ehrModel.familyMedicalHistory),
                    TitleContainer(
                        title: 'Current Medication',
                        content: controller.ehrModel.currentMedication),
                    TitleContainer(
                        title: 'Female pregnancy',
                        content: controller.ehrModel.femalePregnancy),
                    TitleContainer(
                        title: 'Breast Feeding',
                        content: controller.ehrModel.breastFeeding),
                    TitleContainer(
                        title: 'Personification',
                        content: controller.ehrModel.personification),
                    TitleContainer(
                        title: 'Symptoms',
                        content: controller.ehrModel.symptoms),
                    ElevatedButton(
                      child: const Text("data",
                          style: TextStyle(color: Colors.black)),
                      onPressed: () {
                        controller.getEhrShow();
                      },
                    ),
                  ],
                ),
        )

        // AnimationLimiter(
        //   child: ListView.builder(
        //     padding: EdgeInsets.all(_w / 30),
        //     physics: const BouncingScrollPhysics(
        //         parent: AlwaysScrollableScrollPhysics()),
        //     itemCount: 1,
        //     itemBuilder: (BuildContext context, int index) {
        //       return AnimationConfiguration.staggeredList(
        //         position: index,
        //         delay: const Duration(milliseconds: 100),
        //         child: SlideAnimation(
        //           duration: const Duration(milliseconds: 2500),
        //           curve: Curves.fastLinearToSlowEaseIn,
        //           child: FadeInAnimation(
        //             curve: Curves.fastLinearToSlowEaseIn,
        //             duration: const Duration(milliseconds: 2500),
        //             child: Container(
        //               margin: EdgeInsets.only(bottom: _w),
        //               height: _w,
        //               decoration: BoxDecoration(
        //                 color: Colors.cyanAccent,
        //                 borderRadius: const BorderRadius.all(Radius.circular(20)),
        //                 boxShadow: [
        //                   BoxShadow(
        //                     blurStyle: BlurStyle.inner,
        //                     color: Colors.black.withOpacity(0.2),
        //                     blurRadius: 40,
        //                     spreadRadius: 10,
        //                   ),
        //                 ],
        //               ),
        //               child: Column(
        //                 children: [
        //                   Row(
        //                       mainAxisAlignment: MainAxisAlignment.start,
        //                       children: [
        //                         peko(textData: 'Food allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                         SizedBox(
        //                           width: 7,
        //                         ),
        //
        //                       ]),
        //                   Row(
        //                       mainAxisAlignment: MainAxisAlignment.start,
        //                       children: [
        //                         peko(textData: 'Medicine allergies:'),
        //                         SizedBox(
        //                           width: 7,
        //                         ),
        //
        //                         peko(textData: 'Food allergies:'),
        //                       ]),
        //                   Row(
        //                       mainAxisAlignment: MainAxisAlignment.start,
        //                       children: [
        //                         peko(textData: 'Medicine allergies:'),
        //                         SizedBox(
        //                           width: 7,
        //                         ),
        //
        //                         peko(textData: 'Food allergies:'),
        //                       ]),
        //                   Row(
        //                       mainAxisAlignment: MainAxisAlignment.start,
        //                       children: [
        //                         peko(textData: 'Food allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                         SizedBox(
        //                           width: 7,
        //                         ),
        //                         peko(textData: 'Medicine allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                       ]),
        //                   Row(
        //                       mainAxisAlignment: MainAxisAlignment.start,
        //                       children: [
        //                         peko(textData: 'Food allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                         SizedBox(
        //                           width: 7,
        //                         ),
        //                         peko(textData: 'Medicine allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                       ]),
        //                   Row(
        //                       mainAxisAlignment: MainAxisAlignment.start,
        //                       children: [
        //                         peko(textData: 'Food allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                         SizedBox(
        //                           width: 7,
        //                         ),
        //                         peko(textData: 'Medicine allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                       ]),
        //                   Row(
        //                       mainAxisAlignment: MainAxisAlignment.start,
        //                       children: [
        //                         peko(textData: 'Food allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                         SizedBox(
        //                           width: 7,
        //                         ),
        //                         peko(textData: 'Medicine allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                       ]),
        //                   Row(
        //                       mainAxisAlignment: MainAxisAlignment.start,
        //                       children: [
        //                         peko(textData: 'Food allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                         SizedBox(
        //                           width: 7,
        //                         ),
        //                         peko(textData: 'Medicine allergies:'),
        //                         peko(textData: 'Food allergies:'),
        //                       ]),
        //                   Row(
        //                     children: [
        //                       peko(textData: 'Hert disease: '),
        //                       peko(textData: 'Hert disease: '),
        //
        //                       SizedBox(
        //                         width: 7,
        //                       ),
        //                       peko(textData: 'Blood perssure:'),
        //                       peko(textData: 'Blood perssure:')
        //
        //                     ],
        //                   ),
        //                   Row(
        //                     children: [
        //                       Expanded(
        //                         child: Row(
        //                           children: [
        //                             peko(textData: 'Diabetic'),
        //                           ],
        //                         ),
        //                       ),
        //                       Expanded(
        //                         flex: 3,
        //                         child: Row(
        //                           children: [
        //                             peko(textData: 'bleed tendency:'),
        //                             // peko(
        //                             //     textData: stockdetals[i]
        //                             //         .category
        //                             //         .caliber
        //                             //         .toString()),
        //                           ],
        //                         ),
        //                       )
        //                     ],
        //                   ),
        //                   Row(
        //                     children: [
        //                       Row(
        //                         children: [
        //                           peko(textData: 'Surgery:'),
        //                         ],
        //                       ),
        //                       Row(
        //                         children: [
        //                           peko(textData: 'Blood group:'),
        //                           // peko(
        //                           //     textData: stockdetals[i]
        //                           //         .category
        //                           //         .caliber
        //                           //         .toString()),
        //                         ],
        //                       )
        //                     ],
        //                   ),
        //                   Row(
        //                     children: [
        //                       Expanded(
        //                         child: Row(
        //                           children: [
        //                             peko(textData: 'accident:'),
        //                           ],
        //                         ),
        //                       ),
        //                       Row(
        //                         children: [
        //                           peko(textData: 'fa,ily medical history'),
        //                           // peko(
        //                           //     textData: stockdetals[i]
        //                           //         .category
        //                           //         .caliber
        //                           //         .toString()),
        //                         ],
        //                       )
        //                     ],
        //                   ),
        //                   Row(
        //                     children: [
        //                       Row(
        //                         children: [
        //                           peko(textData: 'current medication'),
        //                         ],
        //                       ),
        //                       Row(
        //                         children: [
        //                           peko(textData: 'famale perganancy'),
        //                           // peko(
        //                           //     textData: stockdetals[i]
        //                           //         .category
        //                           //         .caliber
        //                           //         .toString()),
        //                         ],
        //                       )
        //                     ],
        //                   ),
        //                   Row(
        //                     children: [
        //                       Row(
        //                         children: [
        //                           peko(textData: 'breast feeding:'),
        //                         ],
        //                       ),
        //                       Row(
        //                         children: [
        //                           peko(textData: 'personifcation:'),
        //                           // peko(
        //                           //     textData: stockdetals[i]
        //                           //         .category
        //                           //         .caliber
        //                           //         .toString()),
        //                         ],
        //                       )
        //                     ],
        //                   ),
        //                   Row(
        //                     children: [
        //                       Expanded(
        //                         child: Row(
        //                           children: [
        //                             peko(textData: 'symptoms:'),
        //                           ],
        //                         ),
        //                       ),
        //                       Expanded(
        //                         flex: 3,
        //                         child: Row(
        //                           children: [
        //                             peko(textData: 'user_id":'),
        //                             peko(textData: 'user_id'),
        //                             // peko(
        //                             //     textData: stockdetals[i]
        //                             //         .category
        //                             //         .caliber
        //                             //         .toString()),
        //                           ],
        //                         ),
        //                       )
        //                     ],
        //                   ),
        //                 ],
        //               ),
        //             ),
        //           ),
        //         ),
        //       );
        //     },
        //   ),
        // ),
        );
  }
}
