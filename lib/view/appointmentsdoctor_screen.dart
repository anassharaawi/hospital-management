import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:osama_test/global/widgets/titel_container.dart';

import '../controller/appointmentsdoctor_screen_controller.dart';
import '../global/theme/app_colors/app_light_colors.dart';

class AppointmentdoctorScreen extends StatefulWidget {
  const AppointmentdoctorScreen({Key? key}) : super(key: key);

  @override
  State<AppointmentdoctorScreen> createState() => SlideAnimation3();
}

class SlideAnimation3 extends State<AppointmentdoctorScreen> {
  bool gettingData = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppLightColors.primaryColor,
          title: const Text("Appointment Screen"),
          centerTitle: true,
        ),
        body: GetBuilder<AppointmentsdoctorController>(
          init: AppointmentsdoctorController(),
          builder: (AppointmentsdoctorController controller) => controller.isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.separated(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  itemCount: controller.appointmentsModel.length,
                  itemBuilder: (BuildContext context, int index) {
                    return AppointmentCard(
                        doctorName:
                            "${controller.appointmentsModel[index].doctorFirstName} ${controller.appointmentsModel[index].doctorLastName}",
                        department:
                            controller.appointmentsModel[index].department.name,
                        floor: controller
                            .appointmentsModel[index].department.floor,
                        status: controller.appointmentsModel[index].status,
                        date: controller.appointmentsModel[index].date
                            .toString());
                  },

                  separatorBuilder: (BuildContext context, int index) =>
                      const SizedBox(
                    height: 20,
                  ),
                  // children: [
                  //   AppointmentCard(
                  //       doctorName:
                  //           "${controller.appointmentsModel.first.doctorFirstName} ${controller.appointmentsModel.first.doctorLastName}",
                  //       department:
                  //           controller.appointmentsModel.first.department.name,
                  //       floor:
                  //           controller.appointmentsModel.first.department.floor,
                  //       status: controller.appointmentsModel.first.status,
                  //       date:
                  //           controller.appointmentsModel.first.date.toString())
                  // ],
                ),
        ));
  }
}

class AppointmentCard extends StatelessWidget {
  final String doctorName;
  final String department;
  final String floor;
  final String status;
  final String date;
  const AppointmentCard(
      {super.key,
      required this.doctorName,
      required this.department,
      required this.floor,
      required this.status,
      required this.date});

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 200,
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Colors.grey[200],
          boxShadow: [BoxShadow(blurRadius: 3, color: Colors.grey.shade400)],
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          appointmentRecord(
              imagePath: 'assets/images/doctor.png',
              text: doctorName,
              color: Colors.blueGrey,
              fontSize: 22,
              fontWeight: FontWeight.bold),
          const SizedBox(height: 10),
          appointmentRecord(
            imagePath: 'assets/images/dep.png',
            text: "$department - floor $floor ",
          ),
          const SizedBox(height: 10),
          appointmentRecord(
            imagePath: 'assets/images/ok.png',
            text: status,
          ),
          const SizedBox(height: 10),
          appointmentRecord(
            imagePath: 'assets/images/date.png',
            text: date,
          ),
        ],
      ),
    );
  }
}

Row appointmentRecord(
    {required String imagePath,
    required String text,
    Color? color,
    double? fontSize = 18,
    FontWeight? fontWeight}) {
  return Row(
    children: [
      Padding(
        padding: const EdgeInsets.only(right: 20),
        child: SizedBox(
            height: 25,
            width: 25,
            child: Center(
                child: Image.asset(
              imagePath,
            ))),
      ),
      Expanded(
          child: Text(
        text,
        style:
            TextStyle(color: color, fontSize: fontSize, fontWeight: fontWeight),
      ))
    ],
  );
}
