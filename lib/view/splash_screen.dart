import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splashscreen extends StatefulWidget {
  const Splashscreen({super.key});

  @override
  State<Splashscreen> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<Splashscreen> {
  @override
  void initState() {
    SharedPreferences sharedPreferences = Get.find();
    Timer(const Duration(seconds: 3), () {
      bool login = sharedPreferences.containsKey("TOKEN");
      if (login) {
        Get.off('/home');
      } else {
        Get.off('/login');
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.amber,
      body: Center(
        child: Text('Welcome'),
      ),
    );
  }
}
