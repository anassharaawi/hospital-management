import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:osama_test/controller/ehr_screen_controller.dart';
import 'package:osama_test/global/widgets/titel_container.dart';

import '../controller/Appointmentductorsurgery_controller.dart';
import '../controller/AppointmentdoctorpationtScreen_controller.dart';
import '../controller/appoitmentsurgery_screen_controller.dart';
import '../global/theme/app_colors/app_light_colors.dart';
import '../model/appointmentsdocotrpationtModel.dart';

class AppointmentdoctorpationtScreen extends StatefulWidget {
  const AppointmentdoctorpationtScreen({Key? key}) : super(key: key);

  @override
  State<AppointmentdoctorpationtScreen> createState() => SlideAnimation3();
}

class SlideAnimation3 extends State<AppointmentdoctorpationtScreen> {
  bool gettingData = false;

  @override
  Widget build(BuildContext context) {
    double _w = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppLightColors.primaryColor,
          title: Text("Appointment surgery doctor Screen"),
          centerTitle: true,
          systemOverlayStyle: SystemUiOverlayStyle.light,
        ),
        body: GetBuilder<AppointmentsdoctorpationtController>(
            init: AppointmentsdoctorpationtController(),
            builder: (AppointmentsdoctorpationtController controller) =>
                controller.isLoading
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : ListView.builder(
                        itemCount:
                            controller.appointmentsDoctorPationtModel.length,
                        itemBuilder: (context, i) {
                          return Padding(
                           
                              padding: const EdgeInsets.symmetric(
                                vertical: 50, horizontal: 20),
                            child: Column(
                            
                              children: [
                                TitleContainer(
                                  title: 'Surgery date',
                                  content: controller
                                      .appointmentsDoctorPationtModel[i].date
                                      .toString(),
                                ),
                                TitleContainer(
                                  title: 'ductor Name',
                                  content: controller
                                      .appointmentsDoctorPationtModel[i].status
                                      .toString(),
                                ),
                                TitleContainer(
                                  title: 'Surgery description',
                                  content: controller
                                      .appointmentsDoctorPationtModel[i]
                                      .description
                                      .toString(),
                                )
                              ],
                            ),
                          );
                        })));
  }
}
