import 'package:get/get.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';

import '../controller/home_screen_controller.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeScreenController>(
      builder: (controller) {
        return Scaffold(
          body: controller.currentScreen,
          bottomNavigationBar: convexBottomNavBar(),
        );
      },
    );
  }

  Widget convexBottomNavBar() {
    return GetBuilder<HomeScreenController>(
      builder: (controller) => ConvexAppBar(
        backgroundColor: Theme.of(Get.context!).primaryColor,
        activeColor: Colors.white70,
        height: Get.height * 0.06,
        items: const [
          TabItem(
            icon: Icons.assessment,
            title: 'EHR',
          ),
          TabItem(
            icon: Icons.assessment,
            title: 'Patient',
          ),
          TabItem(icon: Icons.assessment, title: 'Appointments'),
        ],
        initialActiveIndex: 1,
        onTap: (index) => controller.changeSelectedValue(index),
      ),
    );
  }
}
