import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:osama_test/controller/ambulance_screen_controller.dart';
import 'package:osama_test/controller/ehr_screen_controller.dart';
import 'package:osama_test/global/widgets/titel_container.dart';
import 'package:geolocator/geolocator.dart';
import '../global/theme/app_colors/app_light_colors.dart';
import '../global/widgets/customized_form_field.dart';

class AmbulanceScreen extends StatefulWidget {
  const AmbulanceScreen();

  @override
  State<AmbulanceScreen> createState() => SlideAnimation3();
}

class SlideAnimation3 extends State<AmbulanceScreen> {
  late ambulanceController amController;
  @override
  void initState() {
    amController = ambulanceController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _w = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppLightColors.primaryColor,
          title: const Text("Ambulance Screen"),
          centerTitle: true,
          systemOverlayStyle: SystemUiOverlayStyle.light,
        ),
        body: GetBuilder<ambulanceController>(
          init: ambulanceController(),
          builder: (ambulanceController controller) => controller.isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 50, horizontal: 20),
                  children: [
                    const SizedBox(
                      height: 50,
                    ),
                    // amFormField(
                    //   textInputType: TextInputType.phone,
                    //   prefixIcon: Icons.phone,
                    //   labelText: "phone number",
                    //   validate: (value) {
                    //     if (value.trim().isEmpty) return 'Required';
                    //   },
                    // ),
                    CustomizedFormField(
                        textInputType: TextInputType.number,
                        prefixIcon: Icons.phone,
                        labelText: 'phone Number',
                        textEditingController: controller.phonenumberController,
                        validate: (value) {
                          if (value.trim().isEmpty) return true;
                        }),

                    const SizedBox(
                      height: 50,
                    ),
                     CustomizedFormField(
                        textInputType: TextInputType.emailAddress,
                        prefixIcon: Icons.local_hospital,
                        labelText: 'Case description',
                        textEditingController: controller.discriptionController,
                        validate: (value) {
                          if (value.trim().isEmpty) return true;
                        }),

                    // amFormField(
                    //   textInputType: TextInputType.text,
                    //   prefixIcon: Icons.local_hospital,
                    //   labelText: "Case description",
                    //   validate: (value) {
                    //     if (value.trim().isEmpty) return 'Required';
                    //   },
                    // ),
                    const SizedBox(width: 150, height: 120),
                    Center(
                      child: SizedBox(
                        width: Get.width * 0.3,
                        height: Get.height * .07,
                        child: controller.isLoading
                            ? const Center(
                                child: CircularProgressIndicator(),
                              )
                            : TextButton(
                                style: TextButton.styleFrom(
                                  backgroundColor: AppLightColors.primaryColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                                onPressed: () {
                                  controller.submit();
                                },
                                child: const Text(
                                  "done",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'opensans',
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 1.1,
                                    fontSize: 18.0,
                                  ),
                                ),
                              ),
                      ),
                    ),
                  ],
                ),
        ));
  }
}
