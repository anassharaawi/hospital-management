import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:osama_test/view/room_booking.dart';
import '../controller/prescriptions_sceen_controller.dart';
import '../global/theme/app_colors/app_light_colors.dart';
import '../model/prescription_model.dart';

class PrescriptionsScreen extends StatelessWidget {
  const PrescriptionsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppLightColors.primaryColor,
          title: const Text("Prescriptions page"),
          centerTitle: true,
        ),
        body: GetBuilder<PrescriptionsController>(
          init: PrescriptionsController(),
          builder: (PrescriptionsController controller) => controller.isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.separated(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  itemCount: controller.prescriptionsModel.length,
                  itemBuilder: (BuildContext context, int index) {
                    return PrescriptionCard(
                      prescription: controller.prescriptionsModel[index],
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const SizedBox(
                    height: 20,
                  ),
                ),
        ));
  }
}

class PrescriptionCard extends StatelessWidget {
  final PrescriptionsModel prescription;
  const PrescriptionCard({
    super.key,
    required this.prescription,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 200,
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Colors.grey[200],
          boxShadow: [BoxShadow(blurRadius: 3, color: Colors.grey.shade400)],
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          cardHeader(),
          const SizedBox(height: 20),
          medicineSection(prescription.medicine),
          medicalAnalysisSectoin(prescription.medicalAnalysis),
          radiographySectoin(prescription.radiograph),
        ],
      ),
    );
  }

  Padding cardHeader() {
    return Padding(
      padding: const EdgeInsets.only(
        left: 5,
      ),
      child: Column(
        children: [
          appointmentRecord(
            imagePath: 'assets/images/doctor.png',
            text:
                '${prescription.doctorFirstName} ${prescription.doctorLastName}',
            color: Colors.blueGrey,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
          const SizedBox(height: 10),
          appointmentRecord(
            imagePath: 'assets/images/date.png',
            color: Colors.blueGrey,
            fontSize: 20,
            text: prescription.date.toString(),
            fontWeight: FontWeight.bold,
          ),
        ],
      ),
    );
  }

  Widget medicalAnalysisSectoin(List<MedicalAnalysis> medicalAnalysis) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              height: 35,
              width: 35,
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.grey,
                  width: 1,
                ),
              ),
              child: Image.asset('assets/images/analysis.png'),
            ),
            const Expanded(
                child: Divider(
              thickness: 1,
              color: Colors.grey,
            )),
          ],
        ),
        const SizedBox(height: 5),
        ...List.generate(medicalAnalysis.length,
            (index) => medicalAnalysisRecord(medicalAnalysis[index]))
      ],
    );
  }

  Widget medicalAnalysisRecord(MedicalAnalysis medicalAnalysis) {
    return ListTile(
      dense: true,
      isThreeLine: false,
      contentPadding: const EdgeInsets.only(left: 10),
      leading: const Icon(
        Icons.donut_large,
        size: 15,
        color: Colors.green,
      ),
      minLeadingWidth: 5,
      title: Text(medicalAnalysis.name),
    );
  }

  Widget radiographySectoin(List<Radiograph> radiographs) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              height: 35,
              width: 35,
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.grey,
                  width: 1,
                ),
              ),
              child: Image.asset('assets/images/radiography.png'),
            ),
            const Expanded(
                child: Divider(
              thickness: 1,
              color: Colors.grey,
            ))
          ],
        ),
        const SizedBox(height: 5),
        ...List.generate(radiographs.length,
            (index) => radiographyRecord(radiographs[index]))
      ],
    );
  }

  Widget radiographyRecord(Radiograph radiograph) {
    return ListTile(
      dense: true,
      isThreeLine: false,
      contentPadding: const EdgeInsets.only(left: 10),
      leading: const Icon(
        Icons.donut_large,
        size: 15,
        color: Colors.green,
      ),
      minLeadingWidth: 5,
      title: Text(radiograph.name),
    );
  }

  Widget medicineSection(List<Medicine> medicines) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              height: 35,
              width: 35,
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  color: Colors.grey,
                  width: 1,
                ),
              ),
              child: Image.asset('assets/images/medicine.png'),
            ),
            const Expanded(
                child: Divider(
              thickness: 1,
              color: Colors.grey,
            ))
          ],
        ),
        const SizedBox(height: 5),
        ...List.generate(
            medicines.length, (index) => medicinRecord(medicines[index]))
      ],
    );
  }

  Widget medicinRecord(Medicine medicine) {
    return ListTile(
      dense: true,
      isThreeLine: true,
      contentPadding: const EdgeInsets.only(left: 10),
      leading: const Icon(
        Icons.donut_large,
        size: 15,
        color: Colors.green,
      ),
      minLeadingWidth: 5,
      title: Text(medicine.name),
      subtitle: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(child: Text('Qty : ${medicine.quantity}')),
          Expanded(child: Text('Dosage :  ${medicine.pivot.dosage}')),
          const Spacer(),
        ],
      ),
    );
  }
}
