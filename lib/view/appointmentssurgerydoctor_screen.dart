import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:osama_test/controller/ehr_screen_controller.dart';
import 'package:osama_test/global/widgets/titel_container.dart';

import '../controller/Appointmentductorsurgery_controller.dart';
import '../controller/AppointmentdoctorpationtScreen_controller.dart';
import '../controller/appoitmentsurgery_screen_controller.dart';
import '../global/theme/app_colors/app_light_colors.dart';

class AppointmentSurgerydoctorScreen extends StatefulWidget {
  const AppointmentSurgerydoctorScreen({Key? key}) : super(key: key);

  @override
  State<AppointmentSurgerydoctorScreen> createState() => SlideAnimation3();
}

class SlideAnimation3 extends State<AppointmentSurgerydoctorScreen> {
  bool gettingData = false;

  @override
  Widget build(BuildContext context) {
    double _w = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppLightColors.primaryColor,
          title: Text("Appointment surgery doctor Screen"),
          centerTitle: true, systemOverlayStyle: SystemUiOverlayStyle.light,
        ),
        body: GetBuilder<AppointmentsdoctorsurgeryController>(
          init: AppointmentsdoctorsurgeryController(),
          builder: (AppointmentsdoctorsurgeryController controller) => controller.isLoading
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : ListView.builder(
                        itemCount:
                            controller.appointmentsDoctorSurgeryModel.length,
                        itemBuilder: (context, i) {
                          return Padding(
                           
                              padding: const EdgeInsets.symmetric(
                                vertical: 50, horizontal: 20),
                            child: Column(
                            
                              children: [
                                TitleContainer(
                                  title: 'Surgery date',
                                  content: controller
                                      .appointmentsDoctorSurgeryModel[i].date
                                      .toString(),
                                ),
                                TitleContainer(
                                  title: 'ductor Name',
                                  content: controller
                                      .appointmentsDoctorSurgeryModel[i].name
                                      .toString(),
                                ),
                                TitleContainer(
                                  title: 'Surgery description',
                                  content: controller
                                      .appointmentsDoctorSurgeryModel[i]
                                      .description
                                      .toString(),
                                )
                              ],
                            ),
                          );
                        })
        ));
  }
}
