import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:osama_test/controller/ehr_screen_controller.dart';
import 'package:osama_test/global/widgets/titel_container.dart';

import '../controller/appoitmentsurgery_screen_controller.dart';
import '../global/theme/app_colors/app_light_colors.dart';

class AppointmentSurgeryScreen extends StatefulWidget {
  const AppointmentSurgeryScreen({Key? key}) : super(key: key);

  @override
  State<AppointmentSurgeryScreen> createState() => SlideAnimation3();
}

class SlideAnimation3 extends State<AppointmentSurgeryScreen> {
  bool gettingData = false;

  @override
  Widget build(BuildContext context) {
    double _w = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppLightColors.primaryColor,
          title: Text("Appointment surgery Screen"),
          centerTitle: true, systemOverlayStyle: SystemUiOverlayStyle.light,
        ),
        body: GetBuilder<AppointmentssurgeryController>(
          init: AppointmentssurgeryController(),
          builder: (AppointmentssurgeryController controller) => controller.isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView(
                  padding:
                      const EdgeInsets.symmetric(vertical: 50, horizontal: 20),
                  children: [
                    TitleContainer(
                      title: 'Surgery Name',
                      content: controller.appointmentSurgeryModel.first.name,
                      
                    ),
                    TitleContainer(
                      title: 'ductor Name',
                      content: controller.appointmentSurgeryModel.first.doctor.user.firstName +" "+controller.appointmentSurgeryModel.first.doctor.user.lastName,
                      
                    ),TitleContainer(
                      title: ' cost',
                      content: controller.appointmentSurgeryModel.first.price,
                      
                    ),
                    TitleContainer(
                      title: 'Surgery date',
                      content: controller.appointmentSurgeryModel.first.date.toString(),
                      
                    )
                  ],
                ),
        ));
  }
}
