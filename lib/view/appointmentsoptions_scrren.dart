import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:osama_test/view/appointmentsdoctor_screen.dart';
import 'package:osama_test/view/room_booking.dart';

import '../global/theme/app_colors/app_light_colors.dart';
import '../global/widgets/drawerwidget.dart';
import 'ambulance.dart';
import 'appoitmentsurgery_screen.dart';

class AppointmentsOptions extends StatelessWidget {
  const AppointmentsOptions({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppLightColors.primaryColor,
          title: const Text("Main Page"),
          centerTitle: true,
        ),
        drawer: const DrwerWidget(),
        body: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              Expanded(
                child: Container(
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: const [BoxShadow(blurRadius: 3)],
                  ),
                  child: InkWell(
                    onTap: () {
                      Get.to(() => const AppointmentdoctorScreen());
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                        color: Colors.white,
                          image: DecorationImage(
                              image: AssetImage('assets/images/clinic.jpg'),
                              opacity: .7,
                              fit: BoxFit.cover)),
                      child: const Center(
                        child: Text(
                          "Clinic Appointments",
                          style: TextStyle(
                              fontSize: 35,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              shadows: [
                                Shadow(color: Colors.white, blurRadius: 3),
                                Shadow(color: Colors.white, blurRadius: 3)
                              ]),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Expanded(
                child: Container(
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: const [BoxShadow(blurRadius: 3)],
                  ),
                  child: InkWell(
                    onTap: () {
                      Get.to(() => const RoomBookingScreen());
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          image: DecorationImage(
                              image: AssetImage(
                                'assets/images/room.jpg',
                              ),
                              opacity: .7,
                              fit: BoxFit.cover)),
                      child: const Center(
                        child: Text(
                          "Room Appointments",
                          style: TextStyle(
                              fontSize: 35,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              shadows: [
                                Shadow(color: Colors.white, blurRadius: 3),
                                Shadow(color: Colors.white, blurRadius: 3)
                              ]),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Expanded(
                child: Container(
                  clipBehavior: Clip.hardEdge,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: const [BoxShadow(blurRadius: 3)],
                  ),
                  child: InkWell(
                    onTap: () {
                      Get.to(() => const AppointmentSurgeryScreen());
                    },
                    child: Container(
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          image: DecorationImage(
                              image:
                                  AssetImage('assets/images/surgery-room.jpg'),
                              opacity: .7,
                              fit: BoxFit.cover)),
                      child: const Center(
                        child: Text(
                          "Surgery Appointments",
                          style: TextStyle(
                              fontSize: 35,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              shadows: [
                                Shadow(color: Colors.white, blurRadius: 3),
                                Shadow(color: Colors.white, blurRadius: 3)
                              ]),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
