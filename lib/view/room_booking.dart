import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:osama_test/global/widgets/titel_container.dart';

import '../controller/appointmentsdoctor_screen_controller.dart';
import '../controller/roombooking_screen_controller.dart';
import '../global/theme/app_colors/app_light_colors.dart';

class RoomBookingScreen extends StatefulWidget {
  const RoomBookingScreen({Key? key}) : super(key: key);

  @override
  State<RoomBookingScreen> createState() => SlideAnimation3();
}

class SlideAnimation3 extends State<RoomBookingScreen> {
  bool gettingData = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppLightColors.primaryColor,
          title: const Text("Room Booking Screen"),
          centerTitle: true,
        ),
        body: GetBuilder<RoomBookingController>(
          init: RoomBookingController(),
          builder: (RoomBookingController controller) => controller.isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView.separated(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                  itemCount: controller.roomBookingModel.length,
                  itemBuilder: (BuildContext context, int index) {
                    return RoomBookingCard(
                        dgree: controller.roomBookingModel.first.rooms.degree,
                        entrydate: controller.roomBookingModel.first.entryDate
                            .toString(),
                        exitdate: controller.roomBookingModel.first.leaveDate
                            .toString(),
                        beds: controller.roomBookingModel.first.rooms.beds
                            .toString(),
                            number: controller.roomBookingModel.first.rooms.number,
                        price: controller.roomBookingModel.first.rooms.price
                            .toString());
                  },

                  separatorBuilder: (BuildContext context, int index) =>
                      const SizedBox(
                    height: 20,
                  ),
                  // children: [
                  //   AppointmentCard(
                  //       doctorName:
                  //           "${controller.appointmentsModel.first.doctorFirstName} ${controller.appointmentsModel.first.doctorLastName}",
                  //       department:
                  //           controller.appointmentsModel.first.department.name,
                  //       floor:
                  //           controller.appointmentsModel.first.department.floor,
                  //       status: controller.appointmentsModel.first.status,
                  //       date:
                  //           controller.appointmentsModel.first.date.toString())
                  // ],
                ),
        ));
  }
}

class RoomBookingCard extends StatelessWidget {
  final String entrydate;
  final String exitdate;
  final String dgree;
  final String beds;
  final String price;
  final String number;

  const RoomBookingCard(
      {super.key,
      required this.entrydate,
      required this.exitdate,
      required this.dgree,
      required this.beds,
      required this.price,
      required this.number});

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 200,
      padding: const EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Colors.grey[200],
          boxShadow: [BoxShadow(blurRadius: 3, color: Colors.grey.shade400)],
          borderRadius: BorderRadius.circular(10)),
      child: Column(
        children: [
          appointmentRecord(
              imagePath: 'assets/images/doctor.png',
              text: dgree,
              color: Colors.blueGrey,
              fontSize: 22,
              fontWeight: FontWeight.bold),
          const SizedBox(height: 10),
          appointmentRecord(
            imagePath: 'assets/images/dep.png',
            text: "$entrydate - floor $exitdate ",
          ),
          const SizedBox(height: 10),
          appointmentRecord(
            imagePath: 'assets/images/ok.png',
            text: beds,
          ),
          const SizedBox(height: 10),
          appointmentRecord(
            imagePath: 'assets/images/date.png',
            text: price,
          ),
          const SizedBox(height: 10),
          appointmentRecord(
            imagePath: 'assets/images/date.png',
            text: number,
          ),
        ],
      ),
    );
  }
}

Row appointmentRecord(
    {required String imagePath,
    required String text,
    Color? color,
    double? fontSize = 18,
    FontWeight? fontWeight}) {
  return Row(
    children: [
      Padding(
        padding: const EdgeInsets.only(right: 20),
        child: SizedBox(
            height: 25,
            width: 25,
            child: Center(
                child: Image.asset(
              imagePath,
            ))),
      ),
      Expanded(
          child: Text(
        text,
        style:
            TextStyle(color: color, fontSize: fontSize, fontWeight: fontWeight),
      ))
    ],
  );
}
